//
//  StoryRecorderController.h
//  RNStoryRecorder
//
//  Created by Dmitry Glushkov on 7/24/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVKit/AVKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

NS_ASSUME_NONNULL_BEGIN

@protocol VideoRecorderDelegate <NSObject>
// метод делегата, срабатывает при успешном завершении видеозаписи
- (void)videoRecorderDidFinishRecordingVideoWithOutputPath:(NSString *)outputPath;

- (void)storyRecorderDidFinishWithData:(NSDictionary *) data;

// метод делегата, срабатывает при отмене видеозаписи
- (void)videoRecorderDidCancelRecordingVideo;
@end

@interface StoryRecorderController : UIViewController

@property (nonatomic, retain) NSString *originaPath;
@property (nonatomic, retain) NSString *processedPath;
@property (nonatomic, retain) NSString *forSavePath;
@property (nonatomic, retain) NSString *compressedPath;


@property (nonatomic, retain) NSString *originaImagePath;
@property (nonatomic, retain) NSString *processedImagePath;
@property (nonatomic, retain) NSString *forSaveImagePath;
@property (nonatomic, retain) NSString *compressedImagePath;

@property (nonatomic, retain) UIImage *originaImage;
@property (nonatomic, retain) UIImage *processedImage;
@property (nonatomic, retain) UIImage *forSaveImage;
@property (nonatomic, retain) UIImage *compressedImage;

@property (nonatomic, retain) UIImage *thumbnailImage;
@property (nonatomic, retain) NSString *thumbnailImagePath;

@property (nonatomic, retain) NSString *captionPlaceholderText;
@property (nonatomic, assign) bool focusIsAllowed;

@property (nonatomic, assign) id<VideoRecorderDelegate> delegate;
@property (nonatomic, assign) bool switchCameraIsAllowed;
@property (nonatomic, assign) bool flashOn;

@property (nonatomic, retain) AVPlayer *player;
@property (nonatomic, retain) AVPlayerLayer *playerLayer;

@property (nonatomic, assign) bool imagePicked;
@property (nonatomic, assign) bool animateRecordButton;

@end

NS_ASSUME_NONNULL_END

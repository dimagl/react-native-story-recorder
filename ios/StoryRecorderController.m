//
//  StoryRecorderController.m
//  RNStoryRecorder
//
//  Created by Dmitry Glushkov on 7/24/19.
//  Copyright © 2019 Facebook. All rights reserved.
//
//
#import "StoryRecorderController.h"
#import "SDAVAssetExportSession.h"
#import "UIImage+fixOrientation.h"
#import "AVAsset+VideoOrientation.h"

#define TOTAL_RECORDING_TIME    15    // максимальное время видеозаписи в секундах
#define FRAMES_PER_SECOND_INPUT 30        // количество кадров в секунду съемки
#define FRAMES_PER_SECOND_OUTPUT 30        // количество кадров в секунду видео файл
#define PLAYER_RATE             1.f     // скорость воспроизведения видео (от 0.0 до 1.0)
#define FREE_DISK_SPACE_LIMIT   1024 * 1024    // минимальный размер свободного места (байт)
#define MAX_VIDEO_FILE_SIZE     100 * 1024 * 1024 * 1024    // максимальный размер видеофайла (байт)
#define CAPTURE_SESSION_PRESET  AVCaptureSessionPresetHigh //качество видеозаписи

#define SAVE_SESSION_PRESET  AVAssetExportPreset1280x720 //качество сохранения

#define SAVE_SESSION_FILE_TYPE   AVFileTypeMPEG4

#define BeginVideoRecording     1117    // звук начала записи видео
#define EndVideoRecording       1118    // звук конца записи видео

@interface StoryRecorderController () <AVCaptureFileOutputRecordingDelegate, AVCapturePhotoCaptureDelegate, UITextViewDelegate, UIGestureRecognizerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    BOOL WeAreRecording;    // флаг, определяющий идет ли запись видео
    
    BOOL MediaIsVideo;
    
    BOOL SelectedMediaHaveAlbumnOrientation;
    BOOL PreviewAsAlbumn;
    
    AVCaptureSession *CaptureSession;
    AVCaptureMovieFileOutput *MovieFileOutput;
    AVCapturePhotoOutput *PhotoOutput;
    AVCaptureDeviceInput *VideoInputDevice;
    AVCaptureDeviceInput *AudioInputDevice;
}

// Эти элементы и методы нужно привязать в дизайнере интерфейса
@property (retain) IBOutlet UILabel *timeLabel;     // индикатор времени записи на верхней панели
@property (retain) IBOutlet UIButton *startButton;     // кнопка Start / Stop

@property (retain) IBOutlet UIButton *cancelButton;      // кнопка Cancel
@property (retain) IBOutlet UIButton *useVideoButton; // кнопка Use Video
//@property (retain) IBOutlet UIView *bottomView;           // нижняя панель
@property (retain) IBOutlet UIButton *playVideoButton; // кнопка Play Video
@property (retain) IBOutlet UIButton *flashButton; // кнопка Flash
@property (retain) IBOutlet UIButton *switchCameraButton; // кнопка switchCamera
@property (retain) IBOutlet UIView *heightLimitView; // height limit view
@property (unsafe_unretained, nonatomic) IBOutlet UIView *takePhototOverlay;

//@property (retain) IBOutlet UITextField *captionTextField;
@property (retain) IBOutlet UITextView *captionTextField;
@property (retain) IBOutlet UIView *buttonsBackground;
@property (retain) IBOutlet UIView *topBackground;
@property (retain) IBOutlet UILabel *addToStoryLabel;
@property (retain) IBOutlet UIButton *saveButton;
@property (retain) IBOutlet UIView *recordingArea;
@property (retain) IBOutlet UIView *timeContainer;
@property (retain) IBOutlet UIImageView *testImage;
@property (retain) IBOutlet UIView *focus;
@property (retain) IBOutlet UIView *savingComplete;
@property (retain) IBOutlet UIImageView *savingCircle;
@property (retain) IBOutlet UIImageView *savingMark;
@property (retain) IBOutlet UIView *processingOverlay;
@property (retain) IBOutlet UILabel *processingOverlayLabel;
@property (retain) IBOutlet UIActivityIndicatorView *useButtonActivityIndicator;
@property (retain) IBOutlet UIImageView *resultImage;
@property (retain) IBOutlet UIView *resultVideo;
@property (retain) IBOutlet UIView *frontFlash;

@property (retain) IBOutlet UIButton *importButton;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *captionTextConstraint;

@property (weak, nonatomic) IBOutlet UIView *displayMediaArea;

@property (weak, nonatomic) IBOutlet UIView *bottomButtonsArea;


@property (weak, nonatomic) IBOutlet UITextField *captionPlaceholder;
@property (weak, nonatomic) IBOutlet UIView *focusArea;
@property (weak, nonatomic) IBOutlet UIView *addToCampusStoryView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelConstraint;


- (IBAction)startStopButtonPressed:(id)sender;     // обработчик нажатия кнопки Start / Stop
- (IBAction)cancel:(id)sender;             // обработчик нажатия кнопки  Cancel
- (IBAction)useVideo:(id)sender;            // обработчик нажатия кнопки  Use Video
- (IBAction)playVideo:(id)sender;            // обработчик нажатия кнопки  Play Video
- (IBAction)flash:(id)sender;               // обработчик нажатия кнопки  Flash
- (IBAction)switchCamera:(id)sender;               // обработчик нажатия кнопки  switchCamera
- (IBAction)captionDidEndEditing:(id)sender;
- (IBAction)save:(id)sender;
- (IBAction)importMedia:(id)sender;

@property (retain) AVCaptureVideoPreviewLayer *PreviewLayer;

// таймер и время для индикатора времени записи
@property (retain) NSTimer *videoTimer;
@property (assign) NSTimeInterval elapsedTime;
@property (assign) AVCaptureDevicePosition CaptureDevice;
@property (retain) UIView *cameraView;

@end

@implementation StoryRecorderController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    SelectedMediaHaveAlbumnOrientation = NO;
    PreviewAsAlbumn = NO;
    
    [self.captionTextField setText:@""];
    [self resetDisplayMediaViewToPortrait];
    
    self.switchCameraIsAllowed = YES;
    // Hide navigation bar.
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.captionTextField.editable = YES;
    
    
    [self.captionTextField setUserInteractionEnabled:YES];
//    self.captionTextField.textContainer.maximumNumberOfLines = 2;
    self.captionTextField.delegate = self;
    [self resetTextView];
    
    UITapGestureRecognizer *tapToFocusRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapToFocusAndExposureRecognizerOrDismissKeyboard:)];
    [self.focusArea addGestureRecognizer:tapToFocusRecognizer];
    
    UITapGestureRecognizer *tapToFlip = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(switchCamera:)];
    tapToFlip.numberOfTapsRequired = 2;
    tapToFlip.delegate = self;
    [self.view addGestureRecognizer:tapToFlip];
    
    [tapToFocusRecognizer requireGestureRecognizerToFail:tapToFlip];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(startRecording:)];
    longPress.delegate = self;
    longPress.minimumPressDuration = 0.3;
    [self.startButton addGestureRecognizer:longPress];
    
    
    UIPinchGestureRecognizer *pinchToZoom = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchToZoomRecognizer:)];
    pinchToZoom.delegate = self;
    [self.recordingArea addGestureRecognizer:pinchToZoom];
    
    self.CaptureDevice = AVCaptureDevicePositionBack;
    
    self.focus.layer.borderWidth = 1;
    self.focus.alpha = 0.8;
    self.focus.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.captionPlaceholderText = @"Tap here to add a caption...";
    
    self.focusIsAllowed = YES;
    self.flashOn = NO;
    
    [self resetTextView];
    [self initVideoDevice];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
    selector: @selector(audioSessionInterruptionEnded:)
    name: AVAudioSessionRouteChangeNotification
    object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
    selector:@selector(willEnterForegroud:)
        name:UIApplicationWillEnterForegroundNotification
      object:nil];
    
    self.captionTextField.layer.shadowColor = [[UIColor clearColor] CGColor];
    self.captionTextField.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    self.captionTextField.layer.shadowOpacity = 0.35f;
    self.captionTextField.layer.shadowRadius = 1.0f;
    self.captionTextField.layer.masksToBounds = NO;
    
    self.cancelButton.titleLabel.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    self.cancelButton.titleLabel.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.cancelButton.titleLabel.layer.shadowRadius = 1.0;
    self.cancelButton.titleLabel.layer.shadowOpacity = 0.35f;
    self.cancelButton.titleLabel.layer.masksToBounds = NO;
    
    self.imagePicked = NO;
    
    self.animateRecordButton = NO;
    
}

-(void)textViewDidChange:(UITextView *)textView
{
    [self fixCaptionPosition];
    NSLog(@"Dilip : %@",textView.text);
}

- (void) willEnterForegroud:(NSNotification *)notification {
    if (self.player) {
        [self fixAudioSession];
        [self.player play];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
    shouldRecognizeSimultaneouslyWithGestureRecognizer:
        (UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}


- (void) fixAudioSession {
    // Initialize audio session
   AVAudioSession *audioSession = [AVAudioSession sharedInstance];

   // Active your audio session
   [audioSession setActive: NO error: nil];

   // Set audio session category
   [audioSession setCategory:AVAudioSessionCategoryPlayback withOptions:AVAudioSessionCategoryOptionMixWithOthers error:nil];


    NSError *setCategoryError = nil;
    if (![audioSession setCategory:AVAudioSessionCategoryPlayback
             withOptions:AVAudioSessionCategoryOptionMixWithOthers
             error:&setCategoryError]) {
        // handle error
        NSLog(@"Log: setCategory error");
    }

    // Old way
   // Modifying Playback Mixing Behavior, allow playing music in other apps
//           OSStatus propertySetError = 0;
//           UInt32 allowMixing = true;
//
//           propertySetError = AudioSessionSetProperty (
//                                  kAudioSessionProperty_OverrideCategoryMixWithOthers,
//                                  sizeof (allowMixing),
//                                  &allowMixing);

   // Active your audio session
   [audioSession setActive: YES error: nil];
}

-(void) handlePinchToZoomRecognizer:(UIPinchGestureRecognizer*)pinchRecognizer {
//    const CGFloat pinchVelocityDividerFactor = 1.0f;

    AVCaptureDevice *VideoDevice = nil; //[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];

    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for(AVCaptureDevice *camera in devices) {
        if([camera position] == self.CaptureDevice) { // is front camera
            VideoDevice = camera;
            break;
        }
    }
    
    
    if (pinchRecognizer.state == UIGestureRecognizerStateChanged) {
        NSError *error = nil;
        if ([VideoDevice lockForConfiguration:&error]) {
            CGFloat desiredZoomFactor = VideoDevice.videoZoomFactor * pinchRecognizer.scale;

            NSLog(@"velocity: %lf", pinchRecognizer.velocity);
            NSLog(@"scale: %lf", pinchRecognizer.scale);
            NSLog(@"VideoDevice.activeFormat.videoMaxZoomFactor: %lf", VideoDevice.activeFormat.videoMaxZoomFactor);
            NSLog(@"desiredZoomFactor: %lf", desiredZoomFactor);
            
            float maxZoom = MIN(VideoDevice.activeFormat.videoMaxZoomFactor, 10.0);
            // Check if desiredZoomFactor fits required range from 1.0 to activeFormat.videoMaxZoomFactor
            VideoDevice.videoZoomFactor = MAX(1.0, MIN(desiredZoomFactor, maxZoom));
            [VideoDevice unlockForConfiguration];
            pinchRecognizer.scale = 1.0;
        } else {
            NSLog(@"error: %@", error);
        }
    }
}

- (void) displayProcessingOverlayWithText:(NSString *) text animate:(BOOL) animate {
    self.processingOverlay.alpha = 0;
    self.processingOverlay.hidden = NO;
    self.processingOverlayLabel.text = text;

    if (animate) {
        [UIView animateWithDuration:0.3
                         animations:^(){
                             self.processingOverlay.alpha = 1;
                         }
                         completion:nil];
    } else {
        self.processingOverlay.alpha = 1;
    }
}

- (void) hideProcessingOverlayWithAnimation:(BOOL) animate {
    if (animate) {
        [UIView animateWithDuration:0.3
                         animations:^(){
                            self.processingOverlay.alpha = 0.0;
                         }
                         completion:^(BOOL completed) {
                            self.processingOverlay.hidden = YES;
                         }];
    } else {
        self.processingOverlay.hidden = YES;
        self.processingOverlay.alpha = 0.0;
    }
}

- (void) initVideoDevice {
    MediaIsVideo = NO;
    self.originaPath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"original.mp4"];
    self.processedPath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"processed.mp4"];
    self.forSavePath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"forSave.mp4"];
    self.compressedPath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"compressed.mp4"];
    
    self.originaImagePath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"originalImage.jpg"];
    self.processedImagePath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"processedImage.jpg"];
    self.forSaveImagePath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"forSaveImage.jpg"];
    self.compressedImagePath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"compressedImage.jpg"];
    
    self.thumbnailImagePath = [[NSString alloc] initWithFormat:@"%@%@", NSTemporaryDirectory(), @"thumbnailImage.jpg"];
    
    [self deleteVideoFile: self.originaPath];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(applicationDidEnterBackground:)
                                                 name: UIApplicationDidEnterBackgroundNotification
                                               object: nil];
    CaptureSession = [[AVCaptureSession alloc] init];
    CaptureSession.sessionPreset = CAPTURE_SESSION_PRESET;
    AVCaptureDevice *VideoDevice = nil; //[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for(AVCaptureDevice *camera in devices) {
        if([camera position] == self.CaptureDevice) { // is front camera
            VideoDevice = camera;
            break;
        }
    }
    
    if (VideoDevice) {
        NSError *error = nil;
        VideoInputDevice = [AVCaptureDeviceInput deviceInputWithDevice:VideoDevice error:&error];
        if (!error) {
            [CaptureSession beginConfiguration];
            if ([CaptureSession canAddInput:VideoInputDevice]) {
                [CaptureSession addInput:VideoInputDevice];
            }
            [CaptureSession commitConfiguration];
        }
    }
    
    AVCaptureDevice *audioDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeAudio];
    NSError *error = nil;
    AudioInputDevice = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:&error];
    
    if (AudioInputDevice) {
        // If the input can be added, add it to the session.
        if ([CaptureSession canAddInput:AudioInputDevice]) {
            [CaptureSession addInput:AudioInputDevice];
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver: self
    selector: @selector(sessionWasInterrupted:)
    name: AVCaptureSessionWasInterruptedNotification
    object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
    selector: @selector(sessionInterruptionEnded:)
    name: AVCaptureSessionInterruptionEndedNotification
    object: nil];
    
    
    [self setPreviewLayer:[[AVCaptureVideoPreviewLayer alloc] initWithSession:CaptureSession]];
    [self.PreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self setupLayoutInRect:[[[self view] layer] bounds]];
    
    self.cameraView = [[UIView alloc] init];
    [[self view] addSubview:self.cameraView];
    [self.view sendSubviewToBack:self.cameraView];

    [[self.cameraView layer] addSublayer:self.PreviewLayer];
    
    MovieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
    CMTime maxDuration = CMTimeMakeWithSeconds(TOTAL_RECORDING_TIME, FRAMES_PER_SECOND_OUTPUT);
    MovieFileOutput.maxRecordedDuration = maxDuration;
    MovieFileOutput.maxRecordedFileSize = MAX_VIDEO_FILE_SIZE;
    MovieFileOutput.minFreeDiskSpaceLimit = FREE_DISK_SPACE_LIMIT;
    MovieFileOutput.movieFragmentInterval = kCMTimeInvalid;
    
    if ([CaptureSession canAddOutput:MovieFileOutput]) {
        [CaptureSession addOutput:MovieFileOutput];
    }
    
    PhotoOutput = [[AVCapturePhotoOutput alloc] init];
    if ([CaptureSession canAddOutput:PhotoOutput]) {
        [CaptureSession addOutput:PhotoOutput];
    }
    
    [self cameraSetOutputProperties];
    [self switchFormatWithDesiredFPS:FRAMES_PER_SECOND_INPUT withOrientation:UIDeviceOrientationPortrait];
}

- (void)audioSessionInterruptionEnded:(NSNotification *)notification {
    
    NSDictionary *prevDevice = [notification.userInfo valueForKeyPath:AVAudioSessionRouteChangePreviousRouteKey];
    NSArray *prevOutputs = [prevDevice valueForKeyPath:@"outputs"];
    
    NSDictionary *prevFirstOutput =  [prevOutputs objectAtIndex:0];
    NSString *prevUID = [prevFirstOutput valueForKeyPath:@"UID"];
    
    NSLog(@"Log: route prev UID: %@", prevUID);
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSArray *currentOutputs = [audioSession.currentRoute valueForKeyPath:@"outputs"];
    NSDictionary *currentFirstOutput =  [currentOutputs objectAtIndex:0];
    
    NSString *currentUID = [currentFirstOutput valueForKeyPath:@"UID"];
    
    NSLog(@"Log: route current UID: %@", currentUID);
    
    if (![prevUID isEqualToString:currentUID])
    {
        if ([prevUID isEqualToString:@"Speaker"] && [currentUID isEqualToString:@"Built-In Receiver"]) {
            self.player.muted = YES;
        }
        if ([prevUID isEqualToString:@"Built-In Receiver"] && [currentUID isEqualToString:@"Speaker"]) {
            self.player.muted = NO;
        }
        
        [self fixAudioSession];
        [self.player play];
    }
}


- (void)sessionWasInterrupted:(NSNotification *)notification {
    NSLog(@"session was interrupted");
    // disconnect the audio device when a call is started
    AVCaptureDevice *device = [AudioInputDevice device];
    if ([device hasMediaType:AVMediaTypeAudio]) {
      [CaptureSession removeInput:AudioInputDevice];
    }
}

- (void)sessionInterruptionEnded:(NSNotification *)notification {
    NSLog(@"session interuption ended");
}

- (IBAction)switchCamera:(id)sender {
    if (WeAreRecording || !self.switchCameraIsAllowed) {
        return;
    }
    
    [self hideFocus];
    
    UIViewAnimationOptions animationOption;
    if(self.CaptureDevice == AVCaptureDevicePositionBack) {
        self.CaptureDevice = AVCaptureDevicePositionFront;
        animationOption = UIViewAnimationOptionTransitionFlipFromRight;
        
//        self.flashOn = NO;
//        [self.flashButton setImage:[UIImage imageNamed:@"flashOn"] forState:UIControlStateNormal];
//
//        [self.flashButton setEnabled:NO];
        [self forceOffFlash];
    } else {
        self.CaptureDevice = AVCaptureDevicePositionBack;
        animationOption = UIViewAnimationOptionTransitionFlipFromLeft;
//        [self.flashButton setEnabled:YES];
    }
    
    [UIView transitionFromView:self.cameraView toView:self.cameraView duration:0.3 options:animationOption completion:NULL];
    
    [CaptureSession stopRunning];

    AVCaptureDevice *VideoDevice = nil; //[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];

    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for(AVCaptureDevice *camera in devices) {
        if([camera position] == self.CaptureDevice) { // is front camera
            VideoDevice = camera;
            break;
        }
    }

    if (VideoDevice) {

        [CaptureSession beginConfiguration];
        [CaptureSession removeInput:VideoInputDevice];
        NSError *error = nil;
        VideoInputDevice = [AVCaptureDeviceInput deviceInputWithDevice:VideoDevice error:&error];
        if (!error) {

            if ([CaptureSession canAddInput:VideoInputDevice]) {
                [CaptureSession addInput:VideoInputDevice];
            }

        }
        [CaptureSession commitConfiguration];
    }

    [self setPreviewLayer:[[AVCaptureVideoPreviewLayer alloc] initWithSession:CaptureSession]];
    [self.PreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [self setupLayoutInRect:[[[self view] layer] bounds]];
    [CaptureSession startRunning];
    self.cameraView = [[UIView alloc] init];
    [[self view] addSubview:self.cameraView];
    [self.view sendSubviewToBack:self.cameraView];

    [[self.cameraView layer] addSublayer:self.PreviewLayer];

    [self switchFormatWithDesiredFPS:FRAMES_PER_SECOND_INPUT withOrientation:UIDeviceOrientationPortrait];

//    [self initVideoDevice];
//    [CaptureSession startRunning];
    
    
    
    [self switchToRecording];
}

- (void)rotateImageView {
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        [self.savingCircle setTransform:CGAffineTransformRotate(self.savingCircle.transform, M_PI_2)];
    }completion:^(BOOL finished){
        if (finished) {
            [self rotateImageView];
        }
    }];
}

- (void) switchSavingButton:(BOOL) saving {
    if (saving) {
        self.saveButton.hidden = YES;
        
        self.savingComplete.hidden = NO;
        
        [self rotateImageView];
    } else {
        self.saveButton.hidden = NO;
        self.savingMark.hidden = YES;
        self.savingComplete.hidden = YES;
    }
}

- (void) savingDidComplete {
    self.savingMark.hidden = NO;
    self.savingMark.alpha = 0;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.savingMark.alpha = 1;
    }completion:nil];
    
    [self.savingCircle.layer removeAllAnimations];
    
    [self performSelector:@selector(switchSavingButton:) withObject:@NO afterDelay:2.0];
}

- (void) displayFocusAtPoint:(CGPoint) point {
    self.focus.transform = CGAffineTransformMakeScale(1.5, 1.5);
    self.focus.center = point;
    self.focus.alpha = 0.8;
    self.focus.hidden = NO;
    
    [UIView animateWithDuration:0.3
                     animations:^(){
                         self.focus.transform = CGAffineTransformMakeScale(1, 1);
                     }
                     completion:nil];
    
    [UIView animateWithDuration:4.0
                     animations:^(){
                         self.focus.alpha = 0.0;
                     }
                     completion:^(BOOL completed) {
                         return;
                     }];
}

- (void)handleTapToFocusAndExposureRecognizerOrDismissKeyboard:(UITapGestureRecognizer*)tabRecognizer {
    [self dismissKeyboard];
    
    if (!self.focusIsAllowed || WeAreRecording) {
        return;
    }
    
    CGPoint touchPoint = [tabRecognizer locationInView:self.view];
    
    [self displayFocusAtPoint:touchPoint];
    
    CGPoint point = [self.PreviewLayer captureDevicePointOfInterestForPoint:touchPoint];
    
    AVCaptureDevice *device = nil;

    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for(AVCaptureDevice *camera in devices) {
        if([camera position] == self.CaptureDevice) { // is front camera
            device = camera;
            break;
        }
    }
    
    if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:AVCaptureFocusModeAutoFocus]) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [device setFocusPointOfInterest:point];
            [device setFocusMode:AVCaptureFocusModeAutoFocus];
            
            [device unlockForConfiguration];
            
            NSLog(@"Log: FOCUS OK");
        } else {
            NSLog(@"Log: FOCUS ERROR = %@", error);
        }
    }
    
    if([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]) {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            [device setExposurePointOfInterest:point];
            [device setExposureMode:AVCaptureExposureModeAutoExpose];
            
            [device unlockForConfiguration];
            NSLog(@"Log: Exposure OK");
        } else {
            NSLog(@"Log: Exposure ERROR = %@", error);
        }
    }
}

-(void)dismissKeyboard {
    [self.captionTextField resignFirstResponder];
}

-(void) resetTextView {
    self.captionTextField.text = self.captionPlaceholderText;
    self.captionTextField.layer.shadowColor = [[UIColor clearColor] CGColor];
    self.captionTextField.textColor = [UIColor colorWithRed:0.70 green:0.75 blue:0.83 alpha:1.0];
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:self.captionPlaceholderText]) {
        textView.text = @"";
        textView.textColor = [UIColor whiteColor];
        self.captionTextField.layer.shadowColor = [[UIColor blackColor] CGColor];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self switchSavingButton: NO];
    
    if ([textView.text isEqualToString:@""]) {
        textView.text = self.captionPlaceholderText;
        self.captionTextField.layer.shadowColor = [[UIColor clearColor] CGColor];
        textView.textColor = [UIColor colorWithRed:0.70 green:0.75 blue:0.83 alpha:1.0];
    }
    [textView resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

// Здесь мы ищем видеоформат наилучшего качества с поддержкой заданного количества кадров в секунду
- (void)switchFormatWithDesiredFPS:(CGFloat)desiredFPS withOrientation:(UIDeviceOrientation) orientation
{
    
    BOOL isRunning = CaptureSession.isRunning;
    
    if (isRunning)  [CaptureSession stopRunning];
    
    AVCaptureDevice *videoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    AVCaptureDeviceFormat *selectedFormat = nil;
    int32_t maxWidth = 0;
    int32_t maxHeight = 0;
    int widthTopLimit = 1920;
    int heightTopLimit = 1080;
    /*if(orientation == UIDeviceOrientationFaceUp ||orientation == UIDeviceOrientationPortraitUpsideDown || orientation == UIDeviceOrientationPortrait){
        widthTopLimit = 1080;
        heightTopLimit = 1920;
    }*/
    AVFrameRateRange *frameRateRange = nil;
    
    for (AVCaptureDeviceFormat *format in [videoDevice formats]) {
        
        for (AVFrameRateRange *range in format.videoSupportedFrameRateRanges) {
            
            CMFormatDescriptionRef desc = format.formatDescription;
            CMVideoDimensions dimensions = CMVideoFormatDescriptionGetDimensions(desc);
            int32_t width = dimensions.width;
            int32_t height = dimensions.height;
            NSLog(@"capture width = %d",   width);
            if (range.minFrameRate <= desiredFPS && desiredFPS <= range.maxFrameRate && width >= maxWidth && height >=maxHeight && width<=widthTopLimit && height <=heightTopLimit) {
                
                selectedFormat = format;
                frameRateRange = range;
                maxWidth = width;
                maxHeight = height;
                NSLog(@"Selected capture format %d x %d",   width,dimensions.height);
                
            }
        }
    }
    
    if (selectedFormat) {
        
        if ([videoDevice lockForConfiguration:nil]) {
            
            NSLog(@"selected format:%@", selectedFormat);
            videoDevice.activeFormat = selectedFormat;
            videoDevice.activeVideoMinFrameDuration = CMTimeMake(1, (int32_t)desiredFPS);
            videoDevice.activeVideoMaxFrameDuration = CMTimeMake(1, (int32_t)desiredFPS);
            
            
            [videoDevice unlockForConfiguration];
        }
    }
    
    if (isRunning) [CaptureSession startRunning];
}


- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    if (WeAreRecording) {
        [self stopRecording];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:self.originaPath]) {
        // Если мы вернулись на экран после просмотра видео, то нам не нужно запускать AVCaptureSession
        WeAreRecording = NO;
        [CaptureSession startRunning];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSLog(@"keyboardSize.height %lf", keyboardSize.height);
    NSLog(@"keyboardSize %@", NSStringFromCGRect([[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue]));
     NSLog(@"self.view.frame %@", NSStringFromCGRect(self.view.frame));
 NSLog(@"self.view.center %@", NSStringFromCGPoint(self.view.center));
    
//    CGPoint f = self.view.center;
//    f.y = 0;
//    self.view.center = f;
    
    CGFloat topPadding = 0.0;
    CGFloat bottomPadding = 0.0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        topPadding = window.safeAreaInsets.top;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
//    CGAffineTransform totalTransform =
//    CGAffineTransformMakeTranslation(0, -keyboardSize.height);
//    self.view.transform =totalTransform;
    [UIView animateWithDuration:0.3 animations:^{
//
//        CGRect f2 = self.resultImage.frame;
//       f2.origin.y = 300;
//       self.resultImage.frame = f2;
//
        CGRect f = [self.view superview].frame;
        f.origin.y = -keyboardSize.height;
        [self.view superview].frame = f;
        
        self.cancelConstraint.constant = keyboardSize.height + 5;
        
        [self.view layoutIfNeeded];
        
        //        CGRect f = self.view.frame;
        //        f.origin.y = 0.0f;
        //        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification {
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = [self.view superview].frame;
        f.origin.y = 0.0;
        [self.view superview].frame = f;
        
        self.cancelConstraint.constant = 5;
        
        [self.view layoutIfNeeded];
//        CGRect f = self.view.frame;
//        f.origin.y = 0.0f;
//        self.view.frame = f;
    }];
}

- (BOOL)shouldAutorotate {
    return (CaptureSession.isRunning && !WeAreRecording);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscape | UIInterfaceOrientationMaskPortraitUpsideDown);
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [self setupLayoutInRect:CGRectMake(0, 0, size.width, size.height)];
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
    } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        [self cameraSetOutputProperties];
    }];
}

// Этот метод выставляет правильную ориентацию файла видео выхода и слоя просмотра
// Он аналогичен viewWillTransitionToSize, нужен для поддержки версий iOS 7 и более ранних
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [self setupLayoutInRect:[[[self view] layer] bounds]];
    [self cameraSetOutputProperties];
}

// Пересчитываем размеры слоя просмотра в зависимости от ориентации устройства
- (void)setupLayoutInRect:(CGRect)layoutRect {
    [self.PreviewLayer setBounds:layoutRect];
    [self.PreviewLayer setPosition:CGPointMake(CGRectGetMidX(layoutRect),  CGRectGetMidY(layoutRect))];
}

// Выставляем правильную ориентацию файла видео выхода и слоя просмотра
- (void)cameraSetOutputProperties {
    AVCaptureConnection *videoConnection = nil;
    for ( AVCaptureConnection *connection in [MovieFileOutput connections] ) {
        for ( AVCaptureInputPort *port in [connection inputPorts] ) {
            if ( [[port mediaType] isEqual:AVMediaTypeVideo] ) {
                videoConnection = connection;
            }
        }
    }
    
    [MovieFileOutput setRecordsVideoOrientationAndMirroringChanges:YES asMetadataTrackForConnection:videoConnection];
    
    if ([videoConnection isVideoOrientationSupported]) {
        if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) {
            self.PreviewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortrait;
            [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortrait];
        }
        else if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown) {
            self.PreviewLayer.connection.videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
            [videoConnection setVideoOrientation:AVCaptureVideoOrientationPortraitUpsideDown];
        }
        else if ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeLeft) {
            self.PreviewLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
            [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeLeft];
        }
        else {
            self.PreviewLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
            [videoConnection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight];
        }
    }
}

- (void) startRecording:(UILongPressGestureRecognizer*) sender {
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        if (WeAreRecording) {
            [self stopRecording];
            MediaIsVideo = YES;
            
            [self scaleDisplayMediaViewToMediaSize:self.view.frame.size];
            
            [self scaleAndMirrorOriginalVideo];
            
            if ([device hasTorch]) {
                [device lockForConfiguration:nil];
                if ([device isTorchActive]) {
                    [device setTorchMode:AVCaptureTorchModeOff];
                }
                [device unlockForConfiguration];
            }
            
        }
    }
    else if (sender.state == UIGestureRecognizerStateBegan){
        if (!WeAreRecording) {
            [self hideFocus];
        
            if (self.flashOn && [device hasTorch] && self.CaptureDevice != AVCaptureDevicePositionFront) {
                [device lockForConfiguration:nil];
                if (![device isTorchActive]) {
                    [device setTorchMode:AVCaptureTorchModeOn];
                }
                [device unlockForConfiguration];
            }
            
            [self startRecording];
        }
    }
    
}

- (void) takePhotoAnimation {
    [self.view bringSubviewToFront:self.takePhototOverlay];
    [self.takePhototOverlay setAlpha:1.0f];
    [UIView animateWithDuration:0.3f animations:^{
        [self.takePhototOverlay setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [self.view sendSubviewToBack:self.takePhototOverlay];
    }];
}

- (void) takePhoto {
//    [self takePhotoAnimation];
    
    if (self.flashOn && self.CaptureDevice == AVCaptureDevicePositionFront) {
        [self displayFrontFlash];
    }
    
    AVCapturePhotoSettings *settings;
    if (@available(iOS 11.0, *)) {
        settings = [AVCapturePhotoSettings photoSettingsWithFormat:@{AVVideoCodecKey: AVVideoCodecTypeJPEG}];
    } else {
        settings = [AVCapturePhotoSettings photoSettingsWithFormat:@{AVVideoCodecKey: AVVideoCodecJPEG}];
    }
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (self.flashOn && [device hasFlash] && self.CaptureDevice != AVCaptureDevicePositionFront) {
        settings.flashMode = AVCaptureFlashModeOn;
    }

    [self scaleDisplayMediaViewToMediaSize:self.view.frame.size];
    
    [PhotoOutput capturePhotoWithSettings:settings delegate:self];
}

//1
- (void)captureOutput:(AVCapturePhotoOutput *)output
willBeginCaptureForResolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings {
    NSLog(@"Log: Image Data willBeginCaptureForResolvedSettings 1");
}

//2
- (void)captureOutput:(AVCapturePhotoOutput *)output
willCapturePhotoForResolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings {
    NSLog(@"Log: Image Data willCapturePhotoForResolvedSettings 2");
}
//3
- (void)captureOutput:(AVCapturePhotoOutput *)output
didCapturePhotoForResolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings {
    NSLog(@"Log: Image Data didCapturePhotoForResolvedSettings 3");
}

//5
- (void)captureOutput:(AVCapturePhotoOutput *)output
didFinishCaptureForResolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings
                error:(NSError *)error {
    NSLog(@"Log: Image Data didFinishCaptureForResolvedSettings 5");
}

- (void)processCapturedImage:(NSData *)imageData {
    [self deleteVideoFile: self.originaImagePath];
    [self deleteVideoFile: self.processedImagePath];
    [self deleteVideoFile: self.forSaveImagePath];
    [self deleteVideoFile: self.compressedImagePath];
    
    if (imageData) {
//        self.captionTextConstraint.constant = self.captionTextConstraint.constant + 100;
//        [self.view layoutIfNeeded];
        
        if (PreviewAsAlbumn) {
            self.resultImage.contentMode = UIViewContentModeScaleAspectFit;
        } else {
            self.resultImage.contentMode = UIViewContentModeScaleAspectFill;
        }
        [self.resultImage setImage:[UIImage imageWithData:imageData]];
        
        if (self.CaptureDevice == AVCaptureDevicePositionFront) {
            self.resultImage.transform = CGAffineTransformMakeScale(-1, 1);
        } else {
            self.resultImage.transform = CGAffineTransformIdentity;
        }
        
        [self.resultImage setHidden:NO];
        
        NSLog(@"Log: Image Data Exists");
        self.originaImage = [UIImage imageWithData:imageData];
        
        self.processedImage = [self scaleImage:self.originaImage];
        if (self.CaptureDevice == AVCaptureDevicePositionFront) {
            self.processedImage = [self flipFrontImage:self.processedImage];
        }
        
        self.thumbnailImage = [self.processedImage copy];
        self.compressedImage = [self.processedImage copy];
        
    } else {
        //        NSLog(@"Log: Image Data Empty");
        //        NSLog(@"%@",error);
        
        [self showProcessingError];
    }
    //    NSLog(@"%@",error);
    [self.useVideoButton setHidden:NO];
    [self.saveButton setHidden:NO];
    self.focusIsAllowed = NO;
    [self hideFocus];
    
    [self stopRecording];
}


-(void)captureOutput:(AVCapturePhotoOutput *)captureOutput didFinishProcessingPhotoSampleBuffer:(CMSampleBufferRef)photoSampleBuffer previewPhotoSampleBuffer:(CMSampleBufferRef)previewPhotoSampleBuffer resolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings bracketSettings:(AVCaptureBracketedStillImageSettings *)bracketSettings error:(NSError *)error
{
    if (error) {
        NSLog(@"error : %@", error.localizedDescription);
    }

    if (self.flashOn) {
        [self hideFrontFlash];
    }
    NSLog(@"Log: Image Data didFinishProcessingPhoto 4");
    
    NSData *imageData = [AVCapturePhotoOutput JPEGPhotoDataRepresentationForJPEGSampleBuffer:photoSampleBuffer previewPhotoSampleBuffer:previewPhotoSampleBuffer];
    
//    NSLog(@"Log: photo: %@", photo);
//    NSLog(@"Log: imageData: %@", imageData);
    
    [self processCapturedImage:imageData];
    
//    if (photoSampleBuffer) {
//        NSData *data = [AVCapturePhotoOutput JPEGPhotoDataRepresentationForJPEGSampleBuffer:photoSampleBuffer previewPhotoSampleBuffer:previewPhotoSampleBuffer];
//        UIImage *image = [UIImage imageWithData:data];
//    }
}

//4
//- (void)captureOutput:(AVCapturePhotoOutput *)output didFinishProcessingPhoto:(AVCapturePhoto *)photo error:(nullable NSError *)error  API_AVAILABLE(ios(11.0)){
//    
//    if (self.flashOn) {
//        [self hideFrontFlash];
//    }
//    NSLog(@"Log: Image Data didFinishProcessingPhoto 4");
//    
//    NSData *imageData = photo.fileDataRepresentation;
//    
//    NSLog(@"Log: photo: %@", photo);
////    NSLog(@"Log: imageData: %@", imageData);
//    
//    [self processCapturedImage:imageData];
//    
////    PhotoOutput = [[AVCapturePhotoOutput alloc] init];
////    if ([CaptureSession canAddOutput:PhotoOutput]) {
////        [CaptureSession addOutput:PhotoOutput];
////    }
//}



- (void) hideFocus {
    self.focus.hidden = YES;
    self.focus.alpha = 0;
}

- (IBAction)startStopButtonPressed:(id)sender {
    if (WeAreRecording) {
        [self stopRecording];
        
        MediaIsVideo = YES;
 
        [self scaleDisplayMediaViewToMediaSize:self.view.frame.size];
        
        [self scaleAndMirrorOriginalVideo];
    } else {
        MediaIsVideo = NO;
        
        [self takePhoto];
        
//        [self stopRecording];
    }
}

- (void) scaleAndMirrorOriginalVideo {
    [self displayProcessingOverlayWithText:@"Processing..." animate:NO];
    
    NSLog(@"Log: start processing");
    
    [self scaleVideoFromPath:self.originaPath output:self.processedPath complition:^(AVAssetExportSession *exportSession) {
        
        NSLog(@"Log: scaleVideoFromPath callback");
        NSLog(@"Log: exportSession.status %@", exportSession.error);
        
        switch(exportSession.status) {
                
            case AVAssetExportSessionStatusCompleted: {
                
                AVURLAsset* asset = [AVURLAsset URLAssetWithURL:[[NSURL alloc] initFileURLWithPath:self.processedPath] options:nil];
                AVAssetImageGenerator* generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:asset];
                generator.appliesPreferredTrackTransform = YES;
                NSError *error = NULL;
                struct CGImage *thImg = [generator copyCGImageAtTime:CMTimeMake(0, 1) actualTime:nil error:&error];
                self.thumbnailImage = [UIImage imageWithCGImage:thImg];
                CGImageRelease(thImg);
                NSLog(@"thumbnail error==%@", error);
                
                //Fix image for upload to server.
                if (self.thumbnailImage.CGImage == nil) {
                    struct CGImage *cgImage =  [[CIContext contextWithOptions:nil] createCGImage:self.thumbnailImage.CIImage fromRect:[self.thumbnailImage.CIImage extent]];
                    self.thumbnailImage = [UIImage imageWithCGImage:cgImage scale:1 orientation:UIImageOrientationUp];
                    
                    CGImageRelease(cgImage);
                }
                
                NSData *thumbnailImageData = UIImagePNGRepresentation(self.thumbnailImage);
                [thumbnailImageData writeToFile:self.thumbnailImagePath atomically:YES];
                

                
                // Compress original video for App.
                CGSize videoSize = [ self getVideoSize:self.processedPath];
                [[NSFileManager defaultManager]  removeItemAtURL: [NSURL fileURLWithPath:self.compressedPath] error:nil];
                
                SDAVAssetExportSession *encoder = [SDAVAssetExportSession.alloc initWithAsset:asset];
                encoder.outputFileType = AVFileTypeMPEG4;
                encoder.outputURL = [[NSURL alloc] initFileURLWithPath:self.compressedPath];
                encoder.videoSettings = @
                {
                AVVideoCodecKey: AVVideoCodecH264,
                AVVideoWidthKey: @(videoSize.width),
                AVVideoHeightKey: @(videoSize.height),
                AVVideoCompressionPropertiesKey: @
                    {
                    AVVideoAverageBitRateKey: @2000000,
                    AVVideoProfileLevelKey: AVVideoProfileLevelH264High41,
                    },
                };
                encoder.audioSettings = @
                {
                AVFormatIDKey: @(kAudioFormatMPEG4AAC),
                AVNumberOfChannelsKey: @2,
                AVSampleRateKey: @44100,
                AVEncoderBitRateKey: @128000,
                };
                
                NSLog(@"Log: Exporting started");
                [encoder exportAsynchronouslyWithCompletionHandler:^
                {
                    
                    if (encoder.status == AVAssetExportSessionStatusCompleted)
                    {
                        NSLog(@"Log: Video export succeeded");
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self hideProcessingOverlayWithAnimation:YES];
                        });
                    }
                    else if (encoder.status == AVAssetExportSessionStatusCancelled)
                    {
                        NSLog(@"Log: Video export cancelled");
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self hideProcessingOverlayWithAnimation:NO];
                            [self showProcessingError];
                        });
                    }
                    else
                    {
                        NSLog(@"Log: Video export failed with error: %@ (%d)", encoder.error.localizedDescription, encoder.error.code);
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self hideProcessingOverlayWithAnimation:NO];
                            [self showProcessingError];
                        });
                    }
                }];
//                CGSize videoSize = [self getVideoSize: self.processedPath];
//
//                NSDictionary *settings = @{AVVideoCodecKey:AVVideoCodecH264,
//                                           AVVideoWidthKey:@(videoSize.width),
//                                           AVVideoHeightKey:@(videoSize.height),
//                                           AVVideoCompressionPropertiesKey:
//                                               @{AVVideoAverageBitRateKey:@(1500000),
//                                                 AVVideoProfileLevelKey:AVVideoProfileLevelH264Main31, /* Or whatever profile & level you wish to use */
//                                                 AVVideoMaxKeyFrameIntervalKey:@(1)}};
//
//                AVAssetWriterInput* writer_input = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo outputSettings:settings];
                
                break;
            }
            default:
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self hideProcessingOverlayWithAnimation:NO];
                    [self showProcessingError];
                });
        }
    }];
}

- (IBAction)cancel:(id)sender {
    if ([CaptureSession isRunning]) {
        if (self.delegate) {
            [self.delegate videoRecorderDidCancelRecordingVideo];
        }
    }
    else {
//        [[self.PreviewLayer connection] setEnabled:YES];
//
//        [self initVideoDevice];
        [self dismissKeyboard];
        
        SelectedMediaHaveAlbumnOrientation = NO;
        PreviewAsAlbumn = NO;
        [self.captionTextField setText:@""];
        [self resetDisplayMediaViewToPortrait];
        
        if (AudioInputDevice) {
            // If the input can be added, add it to the session.
            if ([CaptureSession canAddInput:AudioInputDevice]) {
                [CaptureSession addInput:AudioInputDevice];
            }
        }

        [self switchToRecording];
        [CaptureSession startRunning];

        [self deleteVideoFile: self.originaPath];
        [self deleteVideoFile: self.processedPath];
        [self deleteVideoFile: self.forSavePath];
        [self deleteVideoFile: self.compressedPath];
        
        if (self.player) {
            [self.player pause];
            [self.playerLayer removeFromSuperlayer];
            self.player = nil;
        }
            
        //Clear
        self.originaImage = [[UIImage alloc] init];
        self.processedImage = [[UIImage alloc] init];
        self.thumbnailImage = [[UIImage alloc] init];
        self.compressedImage = [[UIImage alloc] init];
        self.forSaveImage = [[UIImage alloc] init];
        
        
    }
}

- (void) switchToRecording {
    [[self.PreviewLayer connection] setEnabled:YES];
    
    // reset video zoom start
    AVCaptureDevice *VideoDevice = nil; //[AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];

    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for(AVCaptureDevice *camera in devices) {
        if([camera position] == self.CaptureDevice) { // is front camera
            VideoDevice = camera;
            break;
        }
    }
    
    NSError *error = nil;
    if ([VideoDevice lockForConfiguration:&error]) {
        VideoDevice.videoZoomFactor = 1.0;
        [VideoDevice unlockForConfiguration];
    } else {
        NSLog(@"error: %@", error);
    }
    // reset video zoom end

    [self switchSavingButton:NO];
    self.focusIsAllowed = YES;
    
    [self.resultImage setHidden:YES];
    
    self.startButton.hidden = NO;
    self.useVideoButton.hidden = YES;
    self.playVideoButton.hidden = YES;
    self.resultVideo.hidden = YES;
    [self.cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    self.timeLabel.text = @"00:15";
    self.elapsedTime = 15;
    
    self.switchCameraButton.hidden = NO;
    self.importButton.hidden = NO;
    self.flashButton.hidden = NO;
    self.timeContainer.hidden = YES;
    
    self.useButtonActivityIndicator.hidden = YES;
    
    [self.saveButton setHidden:YES];
    [self.addToStoryLabel setHidden:YES];
    [self.addToCampusStoryView setHidden:YES];
    [self.captionTextField setHidden:YES];
    [self.buttonsBackground setHidden:YES];
    [self.topBackground setHidden:YES];
    [self.recordingArea setHidden:NO];
    [self.focusArea setHidden:NO];
    self.switchCameraIsAllowed = YES;
    [self resetTextView];
    
    [self hideProcessingOverlayWithAnimation:YES];
}

- (void) displayFrontFlash {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.frontFlash setHidden:NO];
        [UIView animateWithDuration:0.1 animations:^(void) {
            self.frontFlash.alpha = 1;
        }];
    });
}

- (void) hideFrontFlash {
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.1 animations:^(void) {
            self.frontFlash.alpha = 0;
        } completion:^(BOOL finished){
            [self.frontFlash setHidden:YES];
        }];
    });
}

- (IBAction)importMedia:(id)sender {
    if (self.delegate) {
        
        self.imagePicked = NO;
        NSLog(@"Log: Import pressed");
        
        UIImagePickerController *pickerLibrary = [[UIImagePickerController alloc] init];
        pickerLibrary.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        pickerLibrary.videoMaximumDuration = 15.0f;
        pickerLibrary.allowsEditing = YES;
        pickerLibrary.delegate = self;
        pickerLibrary.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, (NSString *)kUTTypeImage, nil];
//        pickerLibrary.modalPresentationStyle = UIModalPresentationFullScreen;

//        UIViewController *rooterController = [UIApplication sharedApplication].keyWindow.rootViewController;
//        [self.navigationController presentViewController:self.navigationController animated:YES completion:^{
//        }];
//        [self presentModalViewController:pickerLibrary animated:YES];
        [self presentViewController:pickerLibrary animated:YES completion:nil];
        
    }
}

+ (UIInterfaceOrientation)orientationForTrack:(AVAsset *)asset
{
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    if (size.width == txf.tx && size.height == txf.ty && size.width > size.height)
        return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == 0 && size.width > size.height)
        return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == size.width)
        return UIInterfaceOrientationPortraitUpsideDown;
    else
        return UIInterfaceOrientationPortrait;
}

- (void) fixCaptionPosition {
    NSLog(@"Log: fixCaptionPosition");
    if (PreviewAsAlbumn) {
        CGFloat fixedWidth = self.captionTextField.frame.size.width;
        CGSize newSize = [self.captionTextField sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];

        CGFloat limit = self.displayMediaArea.frame.size.height - 10;
        while(newSize.height > limit) {
           [self.captionTextField setText:[self.captionTextField.text substringToIndex:[self.captionTextField.text length] -1]];
           newSize = [self.captionTextField sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        }

        CGRect newFrame = self.captionTextField.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);

        [self.captionTextField setFrame:newFrame];

        [self.captionTextField setCenter:self.displayMediaArea.center];
        [self.captionPlaceholder setCenter:self.displayMediaArea.center];

        CGRect captionTextFrame = self.captionTextField.frame;
        captionTextFrame.origin.y = self.displayMediaArea.frame.size.height - captionTextFrame.size.height - 10;

        [self.captionTextField setFrame:captionTextFrame];
        
        CGRect captionPlaceholderFrame = self.captionPlaceholder.frame;
        captionPlaceholderFrame.origin.y = self.displayMediaArea.frame.size.height - captionPlaceholderFrame.size.height - 10;
        [self.captionPlaceholder setFrame:captionPlaceholderFrame];
        
    } else {
        NSLog(@"Log: fixCaptionPosition !PreviewAsAlbumn");
        CGFloat fixedWidth = self.captionTextField.frame.size.width;
        CGSize newSize = [self.captionTextField sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];

        CGSize screenSize = self.view.frame.size;
        
        // Old recordingArea proportions
        CGFloat limit = screenSize.width / 3 * 4;//self.recordingArea.frame.size.height;
        while(newSize.height > limit) {
            [self.captionTextField setText:[self.captionTextField.text substringToIndex:[self.captionTextField.text length] -1]];
            newSize = [self.captionTextField sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
        }

        CGRect newFrame = self.captionTextField.frame;
        newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
        
        [self.captionTextField setFrame:newFrame];
        
        [self.captionTextField setCenter:self.displayMediaArea.center];
        [self.captionPlaceholder setCenter:self.displayMediaArea.center];
        
        CGRect captionTextFrame = self.captionTextField.frame;
        captionTextFrame.origin.y = self.bottomButtonsArea.frame.origin.y - captionTextFrame.size.height;

        [self.captionTextField setFrame:captionTextFrame];

        CGRect captionPlaceholderFrame = self.captionPlaceholder.frame;
        captionPlaceholderFrame.origin.y = self.bottomButtonsArea.frame.origin.y - captionPlaceholderFrame.size.height;
        [self.captionPlaceholder setFrame:captionPlaceholderFrame];
    }
}

- (void) resetDisplayMediaViewToPortrait {
    CGSize screenSize = self.view.frame.size;
    
    CGRect frame = self.displayMediaArea.frame;
    frame.size.width = screenSize.width;
    frame.size.height =  screenSize.height;
    
    [self.displayMediaArea setFrame:frame];
    [self.displayMediaArea setCenter:self.view.center];

    [self fixCaptionPosition];
}

- (void) scaleDisplayMediaViewToMediaSize:(CGSize) size {
    if (PreviewAsAlbumn) {
        CGSize screenSize = self.view.frame.size;
        
        CGRect frame = self.displayMediaArea.frame;
        CGFloat proportion = screenSize.width / size.width;
        frame.size.width = screenSize.width;
        frame.size.height = size.height * proportion;
        
        [self.displayMediaArea setFrame:frame];
        [self.displayMediaArea setCenter:self.view.center];
        
        [self fixCaptionPosition];
    } else {
        [self resetDisplayMediaViewToPortrait];
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey, id> *)info {

    NSLog(@"Log: Press!");
    
    if (self.imagePicked) {
        return;
    }
    NSLog(@"Log: Press Pass!");
    self.imagePicked = YES;
    
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)); // 1
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){ // 2
        self.imagePicked = NO;
    });
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
        
    //Video.
    if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) {
        
        NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
        NSString *moviePath = [videoUrl path];
        
        AVAsset *avAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:moviePath]];
        
        AVURLAsset *asset = [AVURLAsset URLAssetWithURL:videoUrl options:nil];
        NSArray *tracks = [asset tracksWithMediaType:AVMediaTypeVideo];
        AVAssetTrack *track = [tracks objectAtIndex:0];
        
        NSFileManager * fileManager = [ NSFileManager defaultManager];
        
        // Check if video not longer 15 seconds.
        CMTime time = [asset duration];
        int seconds = ceil(time.value/time.timescale);
        
        if (seconds > 15) {
            [self showVideoLengthError];
            return;
        }
        
        [self deleteVideoFile: self.originaPath];
        [self deleteVideoFile: self.processedPath];
        [self deleteVideoFile: self.forSavePath];
        [self deleteVideoFile: self.compressedPath];
        
        NSError * error = nil;
        [fileManager copyItemAtPath:moviePath toPath:self.originaPath error:&error ];
        
//        [self dismissViewControllerAnimated:YES completion:nil];

        NSLog(@"Log: NEW orientation video %ld", [StoryRecorderController orientationForTrack:avAsset]);

        CGSize mediaSize = track.naturalSize;
        NSLog(@"Log: orientation video width: %f height: %f", mediaSize.width, mediaSize.height);

        NSLog(@"Log: LBVideoOrientation %u", [avAsset videoOrientation]);

        if ([avAsset videoOrientation] == LBVideoOrientationUp || [avAsset videoOrientation] == LBVideoOrientationDown) {
            SelectedMediaHaveAlbumnOrientation = NO;
            NSLog(@"Log: orientation video portrait");
        } else {
            SelectedMediaHaveAlbumnOrientation = YES;
            NSLog(@"Log: orientation video albumn");
        }

        if ([StoryRecorderController orientationForTrack:avAsset] == UIInterfaceOrientationLandscapeRight || [StoryRecorderController orientationForTrack:avAsset] == UIInterfaceOrientationLandscapeLeft) {
            PreviewAsAlbumn = YES;
        } else {
            PreviewAsAlbumn = NO;
        }

        [self scaleDisplayMediaViewToMediaSize:mediaSize];

        // DUPLICATE, CHANGE ALL
        [self stopRecording];
        
        MediaIsVideo = YES;
        
        [self scaleAndMirrorOriginalVideo];
        // DUPLICATE, CHANGE ALL
        
        [self processOriginalVideo];

        NSLog(@"Log: video %@", moviePath);
    // Image.
    } else {
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        NSLog(@"Log: Selected image orientation: %ld", image.imageOrientation);

        image = [image fixOrientation];
        
        NSLog(@"Log: orientation %ld", (long)image.imageOrientation);

        NSLog(@"Log: loaded size width: %f, height: %f", image.size.width, image.size.height);
        
        if (image.size.height > image.size.width) {
            SelectedMediaHaveAlbumnOrientation = NO;
            
            if (image.size.width > 1920) {
                CGFloat scale = image.size.height / image.size.width;
                image = [StoryRecorderController imageWithImage: image scaledToSize:(CGSize)CGSizeMake(1920, 1920 * scale) scale:1];
                NSLog(@"Log: scaled size width: %f, height: %f", image.size.width, image.size.height);
            }
            
            NSLog(@"Log: orientation image portrait");

        } else {
            SelectedMediaHaveAlbumnOrientation = YES;
            
            if (image.size.width > 1920) {
                CGFloat scale = image.size.height / image.size.width;
                image = [StoryRecorderController imageWithImage: image scaledToSize:(CGSize)CGSizeMake(1920, 1920 * scale) scale:1 ];
                
                NSLog(@"Log: scaled size width: %f, height: %f", image.size.width, image.size.height);
            }
            NSLog(@"Log: orientation image albumn");
        }
        
        // Fix
        if (image.CGImage == nil) {
            struct CGImage *cgImage =  [[CIContext contextWithOptions:nil] createCGImage:image.CIImage fromRect:[image.CIImage extent]];
            image = [UIImage imageWithCGImage:cgImage scale:1 orientation:UIImageOrientationUp];
            CGImageRelease(cgImage);
        }
        
        NSData *imageData = UIImagePNGRepresentation(image);
        
        PreviewAsAlbumn = SelectedMediaHaveAlbumnOrientation;
        
        [self scaleDisplayMediaViewToMediaSize:image.size];

//        [self dismissViewControllerAnimated:YES completion:nil];
        
        [self processCapturedImage:imageData];
        
        NSLog(@"Log: image %@", image);
    }
    
    NSLog(@"Log: %@", info);
}

//- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
//{
//    NSLog(@"Log: %@", image);
//}

- (IBAction)useVideo:(id)sender {
    if (self.delegate) {
        
        [self.useButtonActivityIndicator setHidden:NO];
        [self.useVideoButton setHidden:YES];
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
        [data setObject:@"success" forKey:@"status"];
        [data setObject:self.thumbnailImagePath forKey:@"thumbnail"];
        
        if (MediaIsVideo) {
            [data setObject:@"video" forKey:@"type"];
            [data setObject:self.processedPath forKey:@"media"];
            [data setObject:self.compressedPath forKey:@"mediaCompressed"];
            [data setObject:self.processedPath forKey:@"mediaWithCaption"];
            
            CGSize videoSize = [self getVideoSize:self.processedPath];
            [data setObject:@(videoSize.width) forKey:@"width"];
            [data setObject:@(videoSize.height) forKey:@"height"];
            [data setObject:@(PreviewAsAlbumn) forKey:@"landscape"];
            
            [data setObject:[NSNumber numberWithInteger:self.elapsedTime] forKey:@"duration"];
            
            if (![self.captionTextField.text isEqualToString:self.captionPlaceholderText]) {
                
                [data setObject:self.captionTextField.text forKey:@"caption"];
                
                [self addTextToVideo:self.processedPath output:self.forSavePath complition:^(AVAssetExportSession *exportSession) {
                    switch(exportSession.status) {
                        case AVAssetExportSessionStatusCompleted: {
                            
                            [data setObject:self.forSavePath forKey:@"mediaWithCaption"];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.useVideoButton setHidden:NO];
                                [self.useVideoButton setEnabled:NO];
                                [self.useButtonActivityIndicator setHidden:YES];
                                
                                if (self.player) {
                                    [self.player pause];
                                    [self.playerLayer removeFromSuperlayer];
                                    self.player = nil;
                                }
                            });
                            
                            [self.delegate storyRecorderDidFinishWithData:data];
                            
                            break;
                        }
                        default:
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self.useVideoButton setHidden: NO];
                                [self.useButtonActivityIndicator setHidden:YES];
                                [self showSavingError];
                            });
                        
                           
                    }
                }];
            } else {
                
                [data setObject:@"" forKey:@"caption"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.useVideoButton setHidden: NO];
                    [self.useVideoButton setEnabled: NO];
                    [self.useButtonActivityIndicator setHidden: YES];
                    
                    if (self.player) {
                        [self.player pause];
                        [self.playerLayer removeFromSuperlayer];
                        self.player = nil;
                    }
                });
                
                [self.delegate storyRecorderDidFinishWithData:data];
            }
            
        } else {
            [data setObject:@"photo" forKey:@"type"];
 
            [data setObject:[NSNumber numberWithInteger:0] forKey:@"duration"];
            
            NSData *processedImageData = UIImagePNGRepresentation(self.processedImage);
            [processedImageData writeToFile:self.processedImagePath atomically:YES];
            
            NSData *thumbnailImageData = UIImagePNGRepresentation(self.thumbnailImage);
            [thumbnailImageData writeToFile:self.thumbnailImagePath atomically:YES];
            
            NSData *compressedImageData = UIImagePNGRepresentation(self.compressedImage);
            [compressedImageData writeToFile:self.compressedImagePath atomically:YES];
            
            [data setObject:self.processedImagePath forKey:@"media"];
            [data setObject:self.compressedImagePath forKey:@"mediaCompressed"];
            [data setObject:self.processedImagePath forKey:@"mediaWithCaption"];
            
            CGSize imageSize = self.processedImage.size;
            [data setObject:@(imageSize.width) forKey:@"width"];
            [data setObject:@(imageSize.height) forKey:@"height"];
            [data setObject:@(PreviewAsAlbumn) forKey:@"landscape"];
            
            [data setObject:@"" forKey:@"caption"];
            
            if (![self.captionTextField.text isEqualToString:self.captionPlaceholderText]) {
                self.forSaveImage = [self addTextToImage:self.processedImage];
                
                NSData *forSaveImageData = UIImagePNGRepresentation(self.forSaveImage);
                [forSaveImageData writeToFile:self.forSaveImagePath atomically:YES];
                
                [data setObject:self.captionTextField.text forKey:@"caption"];
                [data setObject:self.forSaveImagePath forKey:@"mediaWithCaption"];
                
            }
            
            [self.useVideoButton setHidden: NO];
            [self.useVideoButton setEnabled: NO];
            [self.useButtonActivityIndicator setHidden: YES];
        
            [self.delegate storyRecorderDidFinishWithData:data];
        }
        
        NSLog(@"Log: selected data: %@", data);
    }
}

- (IBAction)playVideo:(id)sender {
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.processedPath]) {
        NSURL *outputFileURL = [[NSURL alloc] initFileURLWithPath:self.processedPath];
        AVPlayer *player = [AVPlayer playerWithURL:outputFileURL];
        AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
        [self presentViewController:controller animated:YES completion:nil];
        controller.player = player;
        controller.allowsPictureInPicturePlayback = NO;
        // здесь мы начинаем воспроизведение видео с заданной скоростью (1 - нормальная скорость, 0.5 - замедленное воспроизведение)
        [player setRate:PLAYER_RATE];
    }
}

- (IBAction)flash:(id)sender {

    if (self.flashOn == YES) {
        self.flashOn = NO;
        [self.flashButton setImage:[UIImage imageNamed:@"flashOn"] forState:UIControlStateNormal];
    } else {
        self.flashOn = YES;
        [self.flashButton setImage:[UIImage imageNamed:@"flashActive"] forState:UIControlStateNormal];
    }
//    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
//    [device lockForConfiguration:nil];
//    [device setFlashMode:AVCaptureFlashModeOn];
//    [device unlockForConfiguration];
//    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
//    if ([device hasTorch]) {
//        [device lockForConfiguration:nil];
//        if ([device isTorchActive]) {
//            [device setTorchMode:AVCaptureTorchModeOff];
//        } else {
//            [device setTorchMode:AVCaptureTorchModeOn];
//        }
//        [device unlockForConfiguration];
//    }
}

- (void) forceOffFlash {
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([device hasTorch]) {
        [device lockForConfiguration:nil];
        [device setTorchMode:AVCaptureTorchModeOff];
        [device unlockForConfiguration];
    }
}

- (void) animateRecordingIncrease:(void(^)(void))complition {
    if (!self.animateRecordButton) {
        complition();
        return;
    }
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.startButton.transform = CGAffineTransformMakeScale(1.25,1.25);
    } completion:^(BOOL finished) {
        [self animateRecordingDecrease: complition];
    }];
}
- (void) animateRecordingDecrease:(void(^)(void))complition {
    [UIView animateWithDuration:0.2 delay:0.2 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.startButton.transform = CGAffineTransformMakeScale(1.0,1.0);
    } completion:^(BOOL finished) {
        [self animateRecordingIncrease:complition];
    }];
}


// Это начало записи видео
- (void)startRecording {
    // Проигрываем звук начала записи видео
    AudioServicesPlaySystemSound(BeginVideoRecording);
    
    [NSThread sleepForTimeInterval: 0.5];
    
    WeAreRecording = YES;
    [self.cancelButton setHidden:YES];
    
    [self.timeContainer setHidden:NO];
    [self.switchCameraButton setHidden:YES];
    [self.importButton setHidden:YES];
    [self.flashButton setHidden:YES];
    
//    [self.bottomView setHidden:YES];
    
    self.animateRecordButton = YES;
    [self animateRecordingIncrease:^{
        [self.startButton setImage:[UIImage imageNamed:@"takePhoto"] forState:UIControlStateNormal];
    }];
    [self.startButton setImage:[UIImage imageNamed:@"takePhotoRed"] forState:UIControlStateNormal];
    self.timeLabel.text = @"00:15";
    self.elapsedTime = 15;
    self.videoTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
    // Удаляем файл видеозаписи, если он существует, чтобы начать запись по новой
    [self deleteVideoFile: self.originaPath];
    [self deleteVideoFile: self.processedPath];
    [self deleteVideoFile: self.forSavePath];
    [self deleteVideoFile: self.compressedPath];

    // Начинаем запись в файл видеозаписи
    NSURL *outputURL = [[NSURL alloc] initFileURLWithPath:self.originaPath];
//    [self deleteVideoFile: self.originaPath];
    [MovieFileOutput startRecordingToOutputFileURL:outputURL recordingDelegate:self];
    
    self.switchCameraIsAllowed = NO;
}

- (void)deleteVideoFile:(NSString *) path {
    [[NSFileManager defaultManager]  removeItemAtURL: [NSURL fileURLWithPath:path] error:nil];
}


- (void) setAsStop {
    WeAreRecording = NO;
    self.startButton.hidden = YES;
    [self.cancelButton setTitle:@"Retake" forState:UIControlStateNormal];
    [self.cancelButton setHidden:NO];
//    [self.bottomView setHidden:NO];
    
    self.animateRecordButton = NO;
//    [self.startButton setImage:[UIImage imageNamed:@"takePhoto"] forState:UIControlStateNormal];
    // останавливаем таймер видеозаписи
    [self.videoTimer invalidate];
    self.videoTimer = nil;
    
    [self.buttonsBackground setHidden:NO];
    [self.topBackground setHidden:NO];
    
    [self.timeContainer setHidden: YES];
    [self.switchCameraButton setHidden:YES];
    [self.importButton setHidden:YES];
    [self.flashButton setHidden:YES];
    
    
    [self.addToStoryLabel setHidden:NO];
    [self.addToCampusStoryView setHidden:NO];
    [self.captionTextField setHidden:NO];
    
    [self.recordingArea setHidden:YES];
    [self.focusArea setHidden:YES];
    
    self.switchCameraIsAllowed = NO;
}
// Это конец записи видео
- (void)stopRecording {
    // Проигрываем звук конца записи видео
    if (WeAreRecording) {
        AudioServicesPlaySystemSound(EndVideoRecording);
    }
    
    [self setAsStop];
    
    
    [CaptureSession stopRunning];
    
    
    // Заканчиваем запись в файл видеозаписи
    [MovieFileOutput stopRecording];
}

- (void)updateTime {
    self.elapsedTime -= self.videoTimer.timeInterval;
    NSInteger seconds = (NSInteger)self.elapsedTime % 60;
    NSInteger minutes = ((NSInteger)self.elapsedTime / 60) % 60;
    self.timeLabel.text = [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

- (void)processOriginalVideo {
    self.useVideoButton.hidden = NO;
    
    //self.playVideoButton.hidden = NO;
    [self.resultVideo setHidden:NO];
    //Play video
    
    
    NSURL *videoURL = [NSURL fileURLWithPath:self.originaPath];
    
    AVPlayerItem *item = [AVPlayerItem playerItemWithURL:videoURL];
    self.player = [AVPlayer playerWithPlayerItem:item];
    self.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.player currentItem]];
    
    CALayer *superlayer = self.resultVideo.layer;
    
    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    [self.playerLayer setFrame:self.resultVideo.bounds];

    if (PreviewAsAlbumn) {
        self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    } else {
        self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    }
    
    if (self.CaptureDevice == AVCaptureDevicePositionFront) {
        self.resultVideo.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    }
    
    [superlayer addSublayer:self.playerLayer];
    
    //        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(playerDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:item];
    
    [self fixAudioSession];
    [self.player seekToTime:kCMTimeZero];
    [self.player play];
    
    [self.saveButton setHidden:NO];
    self.focusIsAllowed = NO;
    
    [self hideFocus];
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput
didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL
      fromConnections:(NSArray *)connections
                error:(NSError *)error {
    if (WeAreRecording) {
        // DUPLICATE, CHANGE ALL
        [self stopRecording];
        
        MediaIsVideo = YES;
        
        [self scaleDisplayMediaViewToMediaSize:self.view.frame.size];
        
        [self scaleAndMirrorOriginalVideo];
        // DUPLICATE, CHANGE ALL
    }
    
    BOOL RecordedSuccessfully = YES;
    if ([error code] != noErr) {
        // Если при записи видео произошла ошибка, но файл был успешно сохранен,
        // то мы все равно считаем, что запись прошла успешно
        id value = [[error userInfo] objectForKey:AVErrorRecordingSuccessfullyFinishedKey];
        if (value != nil) {
            RecordedSuccessfully = [value boolValue];
        }
    }
    if (RecordedSuccessfully) {
        // Если запись прошла успешно, мы показываем кнопку Use Video
        [self processOriginalVideo];
    }
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero completionHandler:nil];
}

- (void)viewDidUnload {
    [super viewDidUnload];
    CaptureSession = nil;
    MovieFileOutput = nil;
    VideoInputDevice = nil;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)captionDidEndEditing:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)save:(id)sender {
    [self switchSavingButton: YES];
    if (MediaIsVideo) {
        
        if (![self.captionTextField.text isEqualToString:self.captionPlaceholderText]) {
            [self addTextToVideo:self.processedPath output:self.forSavePath complition:^(AVAssetExportSession *exportSession) {
                switch(exportSession.status) {
                    case AVAssetExportSessionStatusCompleted: {
                        UISaveVideoAtPathToSavedPhotosAlbum(self.forSavePath, self, @selector(videoSavedToAlbum:didFinishSavingWithError:contextInfo:), nil);
                        break;
                    }
                    default:
                        [self showSavingError];
                }
            }];
        } else {
            UISaveVideoAtPathToSavedPhotosAlbum(self.processedPath, self, @selector(videoSavedToAlbum:didFinishSavingWithError:contextInfo:), nil);
        }
        
    } else {
        if (![self.captionTextField.text isEqualToString:self.captionPlaceholderText]) {
            self.forSaveImage = [self addTextToImage:self.processedImage];
            NSLog(@"Log: Saving width: %f height: %f", self.forSaveImage.size.width, self.forSaveImage.size.height);
            UIImageWriteToSavedPhotosAlbum(self.forSaveImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        } else {
            
            NSLog(@"Log: Saving width: %f height: %f", self.processedImage.size.width, self.processedImage.size.height);
            UIImageWriteToSavedPhotosAlbum(self.processedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        }
    }
}

- (void) showProcessingError {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Processing error"
                                                                   message:@"An error occurred while processing your media"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [self switchToRecording];
                                                              [CaptureSession startRunning];
                                                          }];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) showSavingError {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Saving error"
                                                                   message:@"An error occurred while saving your media"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Retry" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void) showVideoLengthError {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Video processing error"
                                                                   message:@"Video should not be longer than 15 seconds"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (CGSize) getVideoSize: (NSString *) path {
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:[NSURL fileURLWithPath:path] options:nil];
    
    CGSize sizeOfVideo= [[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] naturalSize];
    
    return sizeOfVideo;
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    if (error != nil) {
        [self showSavingError];
        NSLog(@"Log: video.editor.save.error %@",@"Ошибка сохранения фото");
    } else {
        [self savingDidComplete];
        NSLog(@"Log: video.editor.final.save.success %@",@"Фото успешно сохранено в галерее");
    }
}

- (void)videoSavedToAlbum: (NSString *) videoPath didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo{
    if (error) {
        [self showSavingError];
        NSLog(@"Log: video.editor.save.error %@",@"Ошибка сохранения");
    } else {
        [self savingDidComplete];
        NSLog(@"Log: video.editor.final.save.success %@",@"Видео успешно сохранено в галерее");
    }
}

-(UIImage *) fixedOrientation:(UIImage *) image {
    
    NSLog(@"Log: fixedOrientation");
    if (image.imageOrientation == UIImageOrientationUp) {
        return image;
    }
    
    NSLog(@"Log: fixedOrientation pass check, %d", image.imageOrientation);

    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
            
        default: break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            
            NSLog(@"Log: Fix mirrored Up Down");
            CGAffineTransformTranslate(transform, image.size.width, 0);
            CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            
            NSLog(@"Log: Fix mirrored Left Right");
            CGAffineTransformTranslate(transform, image.size.height, 0);
            CGAffineTransformScale(transform, -1, 1);
            break;
            
        default: break;
    }
    
    CGContextRef ctx = CGBitmapContextCreate(nil, image.size.width, image.size.height, CGImageGetBitsPerComponent(image.CGImage), 0, CGImageGetColorSpace(image.CGImage), kCGImageAlphaPremultipliedLast);
    
    CGContextConcatCTM(ctx, transform);
    
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            CGContextDrawImage(ctx, CGRectMake(0, 0, image.size.height, image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0, 0, image.size.width, image.size.height), image.CGImage);
            break;
    }
    
    CGImageRef cgImage = CGBitmapContextCreateImage(ctx);
    
    UIImage *resultImage = [UIImage imageWithCGImage:cgImage];
    CGImageRelease(cgImage);
    CGContextRelease(ctx);
    
    return resultImage;
}


- (UIImage *)addTextToImage:(UIImage *)image
{
    CALayer *overlayContentLayer = [CALayer layer];
    
    CGSize imageSize = image.size;
    
    CGSize screenSize = self.view.frame.size;

//    CGFloat scale = imageSize.width / screenSize.width;
    
//    CGFloat imageScale = imageSize.height / self.recordingArea.frame.size.height;

    CGRect capitionFrame = self.captionTextField.frame;
    
    CGFloat screenProportions = screenSize.width / screenSize.height;
    CGFloat imageProportions = imageSize.width / imageSize.height;

    CGFloat scale = 0 ;
    CGRect frame;

    if (PreviewAsAlbumn) {
        scale = imageSize.width / screenSize.width;
        
        frame = CGRectMake(
            //*****
        (capitionFrame.origin.x + self.captionTextField.textContainerInset.left + self.captionTextField.textContainer.lineFragmentPadding) * scale,
            //*****
          (capitionFrame.origin.y + self.captionTextField.textContainerInset.top) * scale,
            //*****
          (capitionFrame.size.width - self.captionTextField.textContainerInset.left - self.captionTextField.textContainerInset.right - self.captionTextField.textContainer.lineFragmentPadding * 2) * scale,
            //*****
          (capitionFrame.size.height) * scale
            );
    } else {
        if (screenProportions < imageProportions) {
            // Crop left and right.
            NSLog(@"Log: Do Crop LEFT");
            scale = imageSize.height / screenSize.height;
            
            frame = CGRectMake(
                //*****
            (capitionFrame.origin.x + self.captionTextField.textContainerInset.left + self.captionTextField.textContainer.lineFragmentPadding) * scale + (imageSize.width - screenSize.width * scale) / 2,
                //*****
              (capitionFrame.origin.y + self.captionTextField.textContainerInset.top) * scale,
                //*****
              (capitionFrame.size.width - self.captionTextField.textContainerInset.left - self.captionTextField.textContainerInset.right - self.captionTextField.textContainer.lineFragmentPadding * 2) * scale,
                //*****
              (capitionFrame.size.height) * scale
                );
        } else {
            // Crop top and bottom.
            NSLog(@"Log: Do Crop TOP");
            scale = imageSize.width / screenSize.width;
            
            frame = CGRectMake(
                //*****
            (capitionFrame.origin.x + self.captionTextField.textContainerInset.left + self.captionTextField.textContainer.lineFragmentPadding) * scale,
                //*****
              (capitionFrame.origin.y + self.captionTextField.textContainerInset.top) * scale - (imageSize.height - screenSize.height * scale) / 2,
                //*****
              (capitionFrame.size.width - self.captionTextField.textContainerInset.left - self.captionTextField.textContainerInset.right - self.captionTextField.textContainer.lineFragmentPadding * 2) * scale,
                //*****
              (capitionFrame.size.height) * scale
                );
        }
    }

    
    NSLog(@"Log: add text image size: width %f, height %f", imageSize.width, imageSize.height);
    
    overlayContentLayer.frame = CGRectMake(0,0, imageSize.width, imageSize.height);
    overlayContentLayer.opacity = 1.0;
    
    
//    CGRect frame = CGRectMake(
//                              (capitionFrame.origin.x + self.captionTextField.textContainerInset.left + self.captionTextField.textContainer.lineFragmentPadding) * scale,
//                              (capitionFrame.origin.y + self.captionTextField.textContainerInset.top) * scale - self.recordingArea.frame.origin.y * imageScale,
//                              (capitionFrame.size.width - self.captionTextField.textContainerInset.left - self.captionTextField.textContainerInset.right - self.captionTextField.textContainer.lineFragmentPadding * 2)*scale,
//                              (capitionFrame.size.height)*scale
//                              );
    
    CGRect drawFrame = CGRectMake(0, 0, imageSize.width, imageSize.height);
    
    UIGraphicsBeginImageContextWithOptions(drawFrame.size,NO,0.0);
    
    // Draw image.
    [image drawInRect:CGRectMake(0,0,imageSize.width,imageSize.height)];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    UIFont *font = self.captionTextField.font;

    NSShadow *shadow = [NSShadow new];
    [shadow setShadowColor : [UIColor colorWithWhite:0.0f alpha:0.35f]];
    [shadow setShadowOffset : CGSizeMake(0.0f, 0.0f)];
    [shadow setShadowBlurRadius:1 * scale];
    
    NSLog(@"Log: font attributes: %@", [font fontDescriptor]);
    NSLog(@"Log: font scale: %f", scale);
    NSLog(@"Log: font size before: %f", font.pointSize);
    NSLog(@"Log: font size after: %f", font.pointSize * scale);
    NSLog(@"Log: image witdh: %f height: %f", imageSize.width, imageSize.height);

//    CGFloat sc = 23 / 12;
    // Magic! ceil(font.pointSize * scale) + floor(scale)
    //font =  [font fontWithSize:ceil(font.pointSize * scale) + floor(scale)];
    font =  [font fontWithSize:font.pointSize * scale];

    // TODO: Fix kerning
    // NSKernAttributeName

    NSDictionary *attributes = @{ NSFontAttributeName: font,
                                  NSParagraphStyleAttributeName: paragraphStyle,
                                  NSShadowAttributeName: shadow,
                                  NSForegroundColorAttributeName: [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:1.0]};

    NSString *spaces = @"                                                                  ";
    NSString *text = [NSString stringWithFormat:@"%@%@", self.captionTextField.text, spaces];
    [text drawInRect:frame withAttributes:attributes];
    UIImage *newimage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    newimage = [StoryRecorderController imageWithImage: newimage scaledToSize:(CGSize)CGSizeMake(imageSize.width, imageSize.height) scale:1 ];
    
    //Fix image for upload to server.
    if (newimage.CGImage == nil) {
        struct CGImage *cgImage =  [[CIContext contextWithOptions:nil] createCGImage:newimage.CIImage fromRect:[newimage.CIImage extent]];
        newimage = [UIImage imageWithCGImage:cgImage scale:1 orientation:UIImageOrientationUp];
        CGImageRelease(cgImage);
    }
    
    NSLog(@"Log: add text image size after: width %f, height %f", newimage.size.width, newimage.size.height);
    
    return newimage;
}

// Scale 0.0 to use device scale factor
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize scale:(CGFloat) scale {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

//- (UIImage *)addTextToImage:(UIImage *)image
//{
//    CALayer *overlayContentLayer = [CALayer layer];
//
//    CGSize imageSize = image.size;
//
//    CGSize screenSize = self.view.frame.size;
//
//    CGFloat scale = imageSize.width / screenSize.width;
//
//    CGFloat imageScale = imageSize.height / self.recordingArea.frame.size.height;
//
//    CGRect capitionFrame = self.captionTextField.frame;
//
//    overlayContentLayer.frame = CGRectMake(0,0, imageSize.width, imageSize.height);
//    overlayContentLayer.opacity = 1.0;
//
//
//    CGRect frame = CGRectMake(
//                              (capitionFrame.origin.x + self.captionTextField.textContainerInset.left + self.captionTextField.textContainer.lineFragmentPadding) * scale,
//                              (capitionFrame.origin.y + self.captionTextField.textContainerInset.top) * scale - self.recordingArea.frame.origin.y * imageScale,
//                              (capitionFrame.size.width - self.captionTextField.textContainerInset.left - self.captionTextField.textContainerInset.right - self.captionTextField.textContainer.lineFragmentPadding * 2)*scale,
//                              (capitionFrame.size.height)*scale
//                              );
//
//    CGRect drawFrame = CGRectMake(0, 0, imageSize.width, imageSize.height);
//
//    UIGraphicsBeginImageContextWithOptions(drawFrame.size,NO,0.0);
//
//    // Draw image.
//    [image drawInRect:CGRectMake(0,0,imageSize.width,imageSize.height)];
//
//    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
//    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
//    UIFont *font = self.captionTextField.font;
//
//    NSShadow *shadow = [NSShadow new];
//    [shadow setShadowColor : [UIColor colorWithWhite:0.0f alpha:0.35f]];
//    [shadow setShadowOffset : CGSizeMake(0.0f, 0.0f)];
//    [shadow setShadowBlurRadius:1 * scale];
//
//    // Magic! ceil(font.pointSize * scale) + floor(scale)
//    font =  [font fontWithSize:ceil(font.pointSize * scale) + floor(scale)];
//    NSDictionary *attributes = @{ NSFontAttributeName: font,
//                                  NSParagraphStyleAttributeName: paragraphStyle,
//                                  NSShadowAttributeName: shadow,
//                                  NSForegroundColorAttributeName: [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:1.0]};
//
//    NSString *spaces = @"                                                                  ";
//    NSString *text = [NSString stringWithFormat:@"%@%@", self.captionTextField.text, spaces];
//    [text drawInRect:frame withAttributes:attributes];
//    UIImage *newimage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    //Fix image for upload to server.
//    if (newimage.CGImage == nil) {
//        struct CGImage *cgImage =  [[CIContext contextWithOptions:nil] createCGImage:newimage.CIImage fromRect:[newimage.CIImage extent]];
//        newimage = [UIImage imageWithCGImage:cgImage scale:1 orientation:UIImageOrientationUp];
//        CGImageRelease(cgImage);
//    }
//
//    return newimage;
//}

- (UIImage *) flipFrontImage: (UIImage *) image {

    CIImage* coreImage = image.CIImage;
    
    if (!coreImage) {
        coreImage = [CIImage imageWithCGImage:image.CGImage];
    }
    
    coreImage = [coreImage imageByApplyingOrientation: kCGImagePropertyOrientationUpMirrored];
    
    UIImage *newimage = [UIImage imageWithCIImage:coreImage];
    
    //Fix image for upload to server.
    if (newimage.CGImage == nil) {
        struct CGImage *cgImage =  [[CIContext contextWithOptions:nil] createCGImage:newimage.CIImage fromRect:[newimage.CIImage extent]];
        newimage = [UIImage imageWithCGImage:cgImage scale:1 orientation:UIImageOrientationUp];
        CGImageRelease(cgImage);
    }
    
    return newimage;
}

- (UIImage *)scaleImage:(UIImage *)image
{

    image = [self fixedOrientation: image];
    
    // Disable scale and crop.
    return image;
    
    CGSize screenSize = self.view.frame.size;
    CGFloat videoRatio = image.size.width/image.size.height;
    
    CGFloat videoWidth = screenSize.height * videoRatio;
    
    CGFloat scale = videoWidth / screenSize.width;
    
    CIImage* coreImage = image.CIImage;
    
    if (!coreImage) {
        coreImage = [CIImage imageWithCGImage:image.CGImage];
    }
    CGFloat newWidth = image.size.width / scale;
    CGFloat newHeight = image.size.width / scale /2 * 3;
    
    CGFloat imageScale = image.size.height / screenSize.height;
    
    NSLog(@"log: self.recordingArea.frame.origin.y %lf", self.recordingArea.frame.origin.y);
    
    NSLog(@"log: newWidth %lf", newWidth);
    NSLog(@"log: newHeight %lf", newHeight);
    
    NSLog(@"log: scale %lf", scale);
    
    NSLog(@"log: self.recordingArea.frame.origin.y / scale %lf", self.recordingArea.frame.origin.y / scale);
    NSLog(@"log: image.size.width %lf", image.size.width);
    NSLog(@"log: image.size.height %lf", image.size.height);

    // IMPORTANT! self.recordingArea.frame.origin.y will be wrong.
    coreImage = [coreImage imageByCroppingToRect:CGRectMake((image.size.width -newWidth) / 2, image.size.height -newHeight - self.recordingArea.frame.origin.y * imageScale, newWidth, newHeight)];
    
    if (self.CaptureDevice == AVCaptureDevicePositionFront) {
        coreImage = [coreImage imageByApplyingOrientation: kCGImagePropertyOrientationUpMirrored];
    }
    
    UIImage *newimage = [UIImage imageWithCIImage:coreImage];
    
    //Fix image for upload to server.
    if (newimage.CGImage == nil) {
        struct CGImage *cgImage =  [[CIContext contextWithOptions:nil] createCGImage:newimage.CIImage fromRect:[newimage.CIImage extent]];
        newimage = [UIImage imageWithCGImage:cgImage scale:1 orientation:UIImageOrientationUp];
        CGImageRelease(cgImage);
    }
    
    
    return newimage;
}


- (void) addTextToVideo:(NSString *) path output:(NSString *) outputPath complition:(void(^)(AVAssetExportSession *))complition
{
    CALayer *overlayContentLayer = [CALayer layer];

    CGSize videoSize = [self getVideoSize: path];

    CGSize screenSize = self.view.frame.size;

    // CGFloat scale = videoSize.width / screenSize.width;

    // CGFloat imageScale = videoSize.height / self.recordingArea.frame.size.height;

    CGRect capitionFrame = self.captionTextField.frame;

    CGFloat screenProportions = screenSize.width / screenSize.height;
    CGFloat videoProportions = videoSize.width / videoSize.height;
    
    
    CGFloat scale = 0 ;
    CGRect frame;

    if (PreviewAsAlbumn) {
        scale = videoSize.width / screenSize.width;
        
        frame = CGRectMake(
            //*****
        (capitionFrame.origin.x + self.captionTextField.textContainerInset.left + self.captionTextField.textContainer.lineFragmentPadding) * scale,
            //*****
          videoSize.height - (capitionFrame.origin.y + capitionFrame.size.height + self.captionTextField.textContainerInset.top) * scale,
            //*****
          (capitionFrame.size.width - self.captionTextField.textContainerInset.left - self.captionTextField.textContainerInset.right - self.captionTextField.textContainer.lineFragmentPadding * 2) * scale,
            //*****
          (capitionFrame.size.height) * scale
            );
    } else {
        if (screenProportions < videoProportions) {
            // Crop left and right.
            
            NSLog(@"Log: Do Crop LEFT");
            scale = videoSize.height / screenSize.height;
            
            frame = CGRectMake(
                //*****
            (capitionFrame.origin.x + self.captionTextField.textContainerInset.left + self.captionTextField.textContainer.lineFragmentPadding) * scale + (videoSize.width - screenSize.width * scale) / 2,
                //*****
              videoSize.height - (capitionFrame.origin.y  + capitionFrame.size.height + self.captionTextField.textContainerInset.top) * scale,
                //*****
              (capitionFrame.size.width - self.captionTextField.textContainerInset.left - self.captionTextField.textContainerInset.right - self.captionTextField.textContainer.lineFragmentPadding * 2) * scale,
                //*****
              (capitionFrame.size.height) * scale
                );
            
        } else {
            // Crop top and bottom.
            
            NSLog(@"Log: Do Crop TOP");
            scale = videoSize.width / screenSize.width;
            
            frame = CGRectMake(
                //*****
            (capitionFrame.origin.x + self.captionTextField.textContainerInset.left + self.captionTextField.textContainer.lineFragmentPadding) * scale,
                //*****
              videoSize.height - (capitionFrame.origin.y  + capitionFrame.size.height + self.captionTextField.textContainerInset.top) * scale - (videoSize.height - screenSize.height * scale) / 2,
                //*****
              (capitionFrame.size.width - self.captionTextField.textContainerInset.left - self.captionTextField.textContainerInset.right - self.captionTextField.textContainer.lineFragmentPadding * 2) * scale,
                //*****
              (capitionFrame.size.height) * scale
                );
        }
    }
    
    NSLog(@"videoSize.width: %f videoSize.height: %f", videoSize.width, videoSize.height);
    NSLog(@"screenSize.width: %f screenSize.height: %f", screenSize.width, screenSize.height);
    
    overlayContentLayer.frame = CGRectMake(0,0, videoSize.width, videoSize.height);
    overlayContentLayer.opacity = 1.0;

    

    CGRect drawFrame = CGRectMake(0, 0, capitionFrame.size.width * scale, capitionFrame.size.height * scale);

    UIGraphicsBeginImageContextWithOptions(drawFrame.size,NO,0.0);

    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];

    UIFont *font = self.captionTextField.font;

    NSShadow *shadow = [NSShadow new];
    [shadow setShadowColor : [UIColor colorWithWhite:0.0f alpha:0.35f]];
    [shadow setShadowOffset : CGSizeMake(0.0f, 0.0f)];
    [shadow setShadowBlurRadius:1 * scale];

    // Magic! ceil(font.pointSize * scale) + floor(scale)
    font =  [font fontWithSize:ceil(font.pointSize * scale) + floor(scale)];
    NSDictionary *attributes = @{ NSFontAttributeName: font,
                                  NSParagraphStyleAttributeName: paragraphStyle,
                                  NSShadowAttributeName: shadow,
                                  NSForegroundColorAttributeName: [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:1.0]};

    NSString *spaces = @"                                                                  ";
    NSString *text = [NSString stringWithFormat:@"%@%@", self.captionTextField.text, spaces];

    [text drawInRect:drawFrame withAttributes:attributes];
    UIImage *textImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    CALayer *textLayer = [CALayer layer];
    textLayer.contents = (id)textImage.CGImage;
    textLayer.frame = frame;
    textLayer.opacity = 1.0;

    [overlayContentLayer addSublayer:textLayer];

    [self addLayer:overlayContentLayer toVideo:path output:outputPath complition:^(AVAssetExportSession *exportSession) {
complition(exportSession);
    }];
}

//- (void) addTextToVideo:(NSString *) path output:(NSString *) outputPath complition:(void(^)(AVAssetExportSession *))complition
//{
//    CALayer *overlayContentLayer = [CALayer layer];
//
//    CGSize videoSize = [self getVideoSize: path];
//
//    CGSize screenSize = self.view.frame.size;
//
//    CGFloat scale = videoSize.width / screenSize.width;
//
//    CGFloat imageScale = videoSize.height / self.recordingArea.frame.size.height;
//
//    CGRect capitionFrame = self.captionTextField.frame;
//
//    overlayContentLayer.frame = CGRectMake(0,0, videoSize.width, videoSize.height);
//    overlayContentLayer.opacity = 1.0;
//
//    CGRect frame = CGRectMake(
//    (capitionFrame.origin.x + self.captionTextField.textContainerInset.left + self.captionTextField.textContainer.lineFragmentPadding) * scale,
//    videoSize.height - (capitionFrame.origin.y  + capitionFrame.size.height + self.captionTextField.textContainerInset.top) * scale + self.recordingArea.frame.origin.y * imageScale,
//    (capitionFrame.size.width - self.captionTextField.textContainerInset.left - self.captionTextField.textContainerInset.right - self.captionTextField.textContainer.lineFragmentPadding * 2) * scale,
//    (capitionFrame.size.height) * scale
//);
//
//    CGRect drawFrame = CGRectMake(0, 0, capitionFrame.size.width * scale, capitionFrame.size.height * scale);
//
//    UIGraphicsBeginImageContextWithOptions(drawFrame.size,NO,0.0);
//
//    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
//
//    UIFont *font = self.captionTextField.font;
//
//    NSShadow *shadow = [NSShadow new];
//    [shadow setShadowColor : [UIColor colorWithWhite:0.0f alpha:0.35f]];
//    [shadow setShadowOffset : CGSizeMake(0.0f, 0.0f)];
//    [shadow setShadowBlurRadius:1 * scale];
//
//    // Magic! ceil(font.pointSize * scale) + floor(scale)
//    font =  [font fontWithSize:ceil(font.pointSize * scale) + floor(scale)];
//    NSDictionary *attributes = @{ NSFontAttributeName: font,
//                                  NSParagraphStyleAttributeName: paragraphStyle,
//                                  NSShadowAttributeName: shadow,
//                                  NSForegroundColorAttributeName: [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:1.0]};
//
//    NSString *spaces = @"                                                                  ";
//    NSString *text = [NSString stringWithFormat:@"%@%@", self.captionTextField.text, spaces];
//
//    [text drawInRect:drawFrame withAttributes:attributes];
//    UIImage *textImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//
//    CALayer *textLayer = [CALayer layer];
//    textLayer.contents = (id)textImage.CGImage;
//    textLayer.frame = frame;
//    textLayer.opacity = 1.0;
//
//    [overlayContentLayer addSublayer:textLayer];
//
//    [self addLayer:overlayContentLayer toVideo:path output:outputPath complition:^(AVAssetExportSession *exportSession) {
//complition(exportSession);
//    }];
//}

- (void) scaleVideoFromPath:(NSString *) path output:(NSString *) outputPath complition:(void(^)(AVAssetExportSession *))complition
{
    //load our movie Asset
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:path]];
    
    AVAsset *avAsset = [AVAsset assetWithURL:[NSURL fileURLWithPath:path]];
    
    NSArray *videoTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    
    if ([videoTracks count] <= 0) {
        [self showProcessingError];
        return;
    }
    //create an avassetrack with our asset
    AVAssetTrack *clipVideoTrack = [videoTracks objectAtIndex:0];
    
    //create a video composition and preset some settings
    AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.frameDuration = CMTimeMake(1, 30);
    //here we are setting its render size to its height x height (Square)
    
    AVURLAsset *urlAsset = [AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:path] options:nil];
    NSArray *tracks = [urlAsset tracksWithMediaType:AVMediaTypeVideo];
    AVAssetTrack *track = [tracks objectAtIndex:0];
    
    CGSize mediaSize = track.naturalSize;
    
    NSLog(@"Log: orientation save video width: %f height: %f", mediaSize.width, mediaSize.height);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height);
    
            
    if (!SelectedMediaHaveAlbumnOrientation && mediaSize.width > mediaSize.height ) {
        videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.width);
        NSLog(@"Log: orientation rotate to portrait");
        transform = clipVideoTrack.preferredTransform;
    }
    else if (SelectedMediaHaveAlbumnOrientation && mediaSize.height > mediaSize.width) {
        transform = clipVideoTrack.preferredTransform;
    } else if ([avAsset videoOrientation] == LBVideoOrientationLeft) {
        transform = clipVideoTrack.preferredTransform;
    }

//    if ([avAsset videoOrientation] == LBVideoOrientationUp || [avAsset videoOrientation] == LBVideoOrientationDown) {
//        videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.width);
//        NSLog(@"Log: orientation rotate to portrait");
//    } else {
//        videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height);
//        NSLog(@"Log: orientation rotate to albumn");
//    }
//
//
        
    
    //}
    
//    if (mediaSize.width < mediaSize.height && SelectedMediaHaveAlbumnOrientation) {
//        videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height);
//    } else if (SelectedMediaHaveAlbumnOrientation) {
//        videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height);
//        transform = clipVideoTrack.preferredTransform;
//    } else {
//        videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.width);
//        transform = clipVideoTrack.preferredTransform;
//    }
    
    //create a video instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30));
    
    AVMutableVideoCompositionLayerInstruction* transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];

    // 6 - height of top limit line.
    //Here we shift the viewing square up to the TOP of the video so we only see the top
    
    
    // TODO: Rotate portrait recorded or selected video!
    
    //Make sure the square is portrait
    
    [transformer setTransform:transform atTime:kCMTimeZero];
    
    //add the transformer layer instructions, then add to video composition
    instruction.layerInstructions = [NSArray arrayWithObject:transformer];
    videoComposition.instructions = [NSArray arrayWithObject: instruction];
    
    //Remove any prevouis videos at that path
    [[NSFileManager defaultManager]  removeItemAtURL: [NSURL fileURLWithPath:outputPath] error:nil];
    
    //Export
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:asset presetName:SAVE_SESSION_PRESET] ;
    exporter.videoComposition = videoComposition;
    exporter.outputURL = [NSURL fileURLWithPath:outputPath];
    exporter.outputFileType = SAVE_SESSION_FILE_TYPE;
    exporter.shouldOptimizeForNetworkUse = YES;
    
    NSLog(@"Log: exporting started");
    NSLog(@"Log: videoComposition %@", videoComposition);
    
    [exporter exportAsynchronouslyWithCompletionHandler:^(void){
        // Mirror for front camera.
        
        NSLog(@"Log: exporting done");
        NSLog(@"Log: exporting done, %@", exporter);
        
        // *********
        CGSize size = [self getVideoSize: outputPath];
        NSLog(@"Log: VIDEO SIZES scaleVideoFromPath width: %f, height: %f", size.width, size.height);
        // *********
        
        if (self.CaptureDevice == AVCaptureDevicePositionFront) {
            NSLog(@"Log: Mirror started");
            [self mirrorVideoFromPath:self.processedPath output:self.processedPath complition:^(AVAssetExportSession *exportSession) {
                NSLog(@"Log: Mirror done");
                complition(exportSession);
            }];
        } else {
            complition(exporter);
        }
    }];
    
    NSLog(@"Log: exporting error %@", exporter.error);
}


// OLD SCALE


//- (void) scaleVideoFromPath:(NSString *) path output:(NSString *) outputPath complition:(void(^)(AVAssetExportSession *))complition
//{
//
//
////    [self mirrorVideoFromPath:path output:self.processedPath complition:^(AVAssetExportSession *exportSession) {
////        NSLog(@"Log: Mirror done");
////        complition(exportSession);
////    }];
////
////    return;
////
//
//    //load our movie Asset
//    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:path]];
//
//    //create an avassetrack with our asset
//    AVAssetTrack *clipVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
//
//    //create a video composition and preset some settings
//    AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
//    videoComposition.frameDuration = CMTimeMake(1, 30);
//    //here we are setting its render size to its height x height (Square)
//
//    CGSize screenSize = self.view.frame.size;
//
//    // CGFloat screenRatio = screenSize.width/screenSize.height;
//    CGFloat videoRatio = clipVideoTrack.naturalSize.height/clipVideoTrack.naturalSize.width; // 0.749
//
//    CGFloat videoWidth = screenSize.height * videoRatio; // 499.58
//
//    CGFloat scale = videoWidth / screenSize.width; // 1.3329
//
//    CGFloat scale2 = clipVideoTrack.naturalSize.width / screenSize.height; //clipVideoTrack.naturalSize.width / screenSize.height; // 2.48875 /// 1.865067
//
//    // 375 x 667
//
//    NSLog(@"log: clipVideoTrack.naturalSize.height %lf", clipVideoTrack.naturalSize.height); // 1244.000000 width
//    NSLog(@"log: clipVideoTrack.naturalSize.width %lf", clipVideoTrack.naturalSize.width); // 1660.000000 heigh
//
//    NSLog(@"log: scale %lf", scale);
//    NSLog(@"log: scale2 %lf", scale2);
//
//    NSLog(@"log: self.recordingArea.frame %@", NSStringFromCGRect(self.recordingArea.frame));
//    NSLog(@"log: self.recordingArea.frame.y %lf", self.recordingArea.frame.origin.y);
//
//    CGFloat offsetx = (clipVideoTrack.naturalSize.height * scale - clipVideoTrack.naturalSize.height) / 2;
//    //    CGFloat offsety = (clipVideoTrack.naturalSize.width * scale - clipVideoTrack.naturalSize.width) / 2;
//
//    videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.height, clipVideoTrack.naturalSize.height/2 * 3);
//
//    //create a video instruction
//    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
//    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30));
//
//    AVMutableVideoCompositionLayerInstruction* transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];
//
//    // 6 - height of top limit line.
//    //Here we shift the viewing square up to the TOP of the video so we only see the top
//    CGAffineTransform transform = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height + offsetx, - (self.recordingArea.frame.origin.y * scale2) * scale);
//
//    //Make sure the square is portrait
//    transform = CGAffineTransformRotate(transform, M_PI_2);
//    transform = CGAffineTransformScale(transform, scale, scale);
//
//    [transformer setTransform:transform atTime:kCMTimeZero];
//
//    //add the transformer layer instructions, then add to video composition
//    instruction.layerInstructions = [NSArray arrayWithObject:transformer];
//    videoComposition.instructions = [NSArray arrayWithObject: instruction];
//
//    //Remove any prevouis videos at that path
//    [[NSFileManager defaultManager]  removeItemAtURL: [NSURL fileURLWithPath:outputPath] error:nil];
//
//    //Export
//    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:asset presetName:SAVE_SESSION_PRESET] ;
//    exporter.videoComposition = videoComposition;
//    exporter.outputURL = [NSURL fileURLWithPath:outputPath];
//    exporter.outputFileType = SAVE_SESSION_FILE_TYPE;
//    exporter.shouldOptimizeForNetworkUse = YES;
//
//    NSLog(@"Log: exporting started");
//    NSLog(@"Log: videoComposition %@", videoComposition);
//
//    [exporter exportAsynchronouslyWithCompletionHandler:^(void){
//        // Mirror for front camera.
//
//        NSLog(@"Log: exporting done");
//        NSLog(@"Log: exporting done, %@", exporter);
//
//        if (self.CaptureDevice == AVCaptureDevicePositionFront) {
//            NSLog(@"Log: Mirror started");
//            [self mirrorVideoFromPath:self.processedPath output:self.processedPath complition:^(AVAssetExportSession *exportSession) {
//                NSLog(@"Log: Mirror done");
//                complition(exportSession);
//            }];
//        } else {
//            complition(exporter);
//        }
//    }];
//
//    NSLog(@"Log: exporting error %@", exporter.error);
//}

- (void) mirrorVideoFromPath:(NSString *) path output:(NSString *) outputPath complition:(void(^)(AVAssetExportSession *))complition
{
    NSLog(@"Log: mirrorVideoFromPath");
    
    //load our movie Asset
    AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:path]];
    
    NSArray *videoTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    
    if ([videoTracks count] <= 0) {
        [self showProcessingError];
        return;
    }
    
    //create an avassetrack with our asset
    AVAssetTrack *clipVideoTrack = [videoTracks objectAtIndex:0];
    
    NSLog(@"Log: clipVideoTrack.naturalSize %@", NSStringFromCGSize(clipVideoTrack.naturalSize));
    
    //create a video composition and preset some settings
    AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.frameDuration = CMTimeMake(1, 30);
    //here we are setting its render size to its height x height (Square)
    
    videoComposition.renderSize = CGSizeMake(clipVideoTrack.naturalSize.width, clipVideoTrack.naturalSize.height);
    
    //create a video instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30));
    
    AVMutableVideoCompositionLayerInstruction* transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];
    
    //Here we shift the viewing square up to the TOP of the video so we only see the top
    CGAffineTransform transform = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.width, 0 );
    transform = CGAffineTransformScale(transform, -1.0, 1.0);
    
    [transformer setTransform:transform atTime:kCMTimeZero];
    
    //add the transformer layer instructions, then add to video composition
    instruction.layerInstructions = [NSArray arrayWithObject:transformer];
    videoComposition.instructions = [NSArray arrayWithObject: instruction];
    
    //Remove any prevouis videos at that path
    [[NSFileManager defaultManager]  removeItemAtURL: [NSURL fileURLWithPath:outputPath] error:nil];
    
    //Export
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:asset presetName:SAVE_SESSION_PRESET] ;
    exporter.videoComposition = videoComposition;
    exporter.outputURL = [NSURL fileURLWithPath:outputPath];
    exporter.outputFileType = SAVE_SESSION_FILE_TYPE;
    exporter.shouldOptimizeForNetworkUse = YES;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^(void){
        
        // *********
        CGSize size = [self getVideoSize: outputPath];
        NSLog(@"Log: VIDEO SIZES mirrorVideoFromPath width: %f, height: %f", size.width, size.height);
        // *********
        
        complition(exporter);
    }];
}

- (void) addLayer:(CALayer *) layer toVideo:(NSString *) path output:(NSString *) outputPath complition:(void(^)(AVAssetExportSession *))complition
{
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:[NSURL fileURLWithPath:path] options:nil];
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    
    AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    
    NSArray<AVAssetTrack *> *videoTracks = [videoAsset tracksWithMediaType:AVMediaTypeVideo];
    if ([videoTracks count] < 1) {
        [self showProcessingError];
        return;
    }
    AVAssetTrack *clipVideoTrack = [videoTracks objectAtIndex:0];
    
    AVAssetTrack *clipAudioTrack = nil;
    // Do not add audio, if audio track not exists
    NSArray<AVAssetTrack *> *audioTracks = [videoAsset tracksWithMediaType:AVMediaTypeAudio];
    if ([audioTracks count] > 0) {
        clipAudioTrack = [[videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    }
    
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration) ofTrack:clipVideoTrack atTime:kCMTimeZero error:nil];
    
    if (clipAudioTrack != nil) {
        AVMutableCompositionTrack *compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration) ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];
    }
    
    [compositionVideoTrack setPreferredTransform:[[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] preferredTransform]];
    
    CGSize sizeOfVideo = clipVideoTrack.naturalSize;
    
    CALayer *parentLayer=[CALayer layer];
    CALayer *videoLayer=[CALayer layer];
    parentLayer.frame=CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
    videoLayer.frame=CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:layer];
    
    AVMutableVideoComposition *videoComposition=[AVMutableVideoComposition videoComposition] ;
    videoComposition.frameDuration=CMTimeMake(1, 30);
    videoComposition.renderSize=sizeOfVideo;
    videoComposition.animationTool=[AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [mixComposition duration]);
    AVAssetTrack *videoTrack = [[mixComposition tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVMutableVideoCompositionLayerInstruction* layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
    videoComposition.instructions = [NSArray arrayWithObject: instruction];
    
    [[NSFileManager defaultManager]  removeItemAtURL: [NSURL fileURLWithPath:outputPath] error:nil];
    
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:SAVE_SESSION_PRESET];
    exportSession.videoComposition=videoComposition;
    
    exportSession.outputURL = [NSURL fileURLWithPath:outputPath];
    exportSession.outputFileType = SAVE_SESSION_FILE_TYPE;
    exportSession.shouldOptimizeForNetworkUse = YES;
    
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        
        // *********
        CGSize size = [self getVideoSize: outputPath];
        NSLog(@"Log: VIDEO SIZES addLayer width: %f, height: %f", size.width, size.height);
        // *********
        
        complition(exportSession);
    }];
}


@end

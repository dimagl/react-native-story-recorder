//
//  UIImage+fixOrientation.h
//  RNStoryRecorder
//
//  Created by Dmitry Glushkov on 2/14/20.
//  Copyright © 2020 Facebook. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end

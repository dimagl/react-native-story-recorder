#if __has_include(<React/RCTBridgeModule.h>)
#import <React/RCTBridgeModule.h>
#else
#import "RCTBridgeModule.h"
#endif

#import "StoryRecorderController.h"

@interface RNStoryRecorder : NSObject <RCTBridgeModule, VideoRecorderDelegate>

@property (nonatomic, retain) UINavigationController *navigationController;

@property (nonatomic, copy) RCTPromiseResolveBlock resolve;
@property (nonatomic, copy) RCTPromiseRejectBlock reject;

@end

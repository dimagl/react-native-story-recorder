#import "RNStoryRecorder.h"
#import "StoryRecorderController.h"
//#import "AppDelegate.h"

@implementation RNStoryRecorder

RCT_EXPORT_MODULE()

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

- (void) fixAudioSessioSession {
     // Initialize audio session
       AVAudioSession *audioSession = [AVAudioSession sharedInstance];

       // Active your audio session
       [audioSession setActive: NO error: nil];

       // Set audio session category
       [audioSession setCategory:AVAudioSessionCategorySoloAmbient error:nil];

        NSError *setCategoryError = nil;
        if (![audioSession setCategory:AVAudioSessionCategorySoloAmbient
                 error:&setCategoryError]) {
            // handle error
            NSLog(@"Log: setCategory error");
        }
       [audioSession setActive: YES error: nil];
}

- (void)videoRecorderDidCancelRecordingVideo {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Log: Close!");
        [self fixAudioSessioSession];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
        self.resolve(@{ @"status": @"closed" });
    });
}

- (void) storyRecorderDidFinishWithData:(NSDictionary *) data {
    dispatch_async(dispatch_get_main_queue(), ^{
        NSLog(@"Log: Export!");
        [self fixAudioSessioSession];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
        self.resolve(data);
    });
}

- (void) openCamera{
    NSLog(@"Log: open pressed");
        dispatch_async(dispatch_get_main_queue(), ^{

            UIStoryboard *storyboardStoryRecorder = [UIStoryboard storyboardWithName:@"StoryRecorder" bundle:nil];
            StoryRecorderController *storyRecorder = [storyboardStoryRecorder instantiateInitialViewController];
            storyRecorder.delegate = self;
            
            UIViewController *rooterController = [UIApplication sharedApplication].keyWindow.rootViewController;
    //        self.navigationController =  [[UINavigationController alloc] initWithRootViewController:storyRecorder];
            UIViewController *cont = [[UIViewController alloc] init];
            
    //        spinner.center.x -=100;
            [cont.view setBackgroundColor:[UIColor blackColor]];
            cont.modalPresentationStyle = UIModalPresentationFullScreen;
            self.navigationController =  [[UINavigationController alloc] initWithRootViewController:cont];
    //         self.navigationController.navigationBarHidden = YES;
    //        [self.navigationController
    //        self.navigationController =  [UINavigationController alloc] ;
            
            //UINavigationBar *bar = [self.navigationController navigationBar];
            //[bar setTintColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0]];
            
            [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
            [[UINavigationBar appearance] setTranslucent:NO];
            
            self.navigationController.modalPresentationStyle = UIModalPresentationFullScreen;
            
            
            UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            [cont.view addSubview:spinner];

            [spinner startAnimating];
            spinner.center =CGPointMake(rooterController.view.center.x,rooterController.view.center.y - self.navigationController.navigationBar.frame.size.height - 30); ;
            //[rooterController.navigationController setNavigationBarHidden:YES animated:YES];
            
            [rooterController presentViewController:self.navigationController animated:YES completion:^{
                NSLog(@"in blocks");
                
                double delayInMSeconds = 50;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInMSeconds * NSEC_PER_MSEC)); // 1
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){ // 2
                   UIStoryboard *storyboardStoryRecorder = [UIStoryboard storyboardWithName:@"StoryRecorder" bundle:nil];
                                    StoryRecorderController *storyRecorder = [storyboardStoryRecorder instantiateInitialViewController];
                                    storyRecorder.delegate = self;
                    //                storyRecorder.modalPresentationStyle = UIModalPresentationFullScreen;

                            [self.navigationController setViewControllers:@[storyRecorder] animated: NO];
                    
                        
                });
                        
            }];
            
        });
}

- (void) noPermissionsCamera{
    [self noPermissionsAlertWithTitle: @"Can't access camera" andText: @"To take photo or video, go to phone's Settings and allow access to camera for Frosh app." andAction:^(){}];
}

- (void) noPermissionsMicrophoneWithAction: (void (^)(void)) action{
    [self noPermissionsAlertWithTitle: @"Can't access microphone" andText: @"Video will have no audio, go to phone's Settings and allow access to microphone for Frosh app." andAction:action];
}


-(void) noPermissionsAlertWithTitle: (NSString *) title andText: (NSString *) text andAction:(void (^)(void)) action {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                       message: text
                                       preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler: ^(UIAlertAction * alertAction) {
            action();
        }];

        [alert addAction:defaultAction];
        UIViewController *rooterController = [UIApplication sharedApplication].keyWindow.rootViewController;
        [rooterController presentViewController:alert animated:YES completion:nil];
    });
}

RCT_REMAP_METHOD(open, openWithResolver:(RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject) {

    self.resolve = resolve;
    self.reject = reject;

    NSString *mediaType = AVMediaTypeVideo;
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if(authStatus == AVAuthorizationStatusAuthorized) {
        NSLog(@"Log: AVAuthorizationStatusAuthorized %@", mediaType);
      // do your logic
        
        [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
            if (granted) {
                NSLog(@"Mic Permission granted");
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openCamera];
                });
            }
            else {
                [self noPermissionsMicrophoneWithAction:^(){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self openCamera];
                    });
                }];
                NSLog(@"Mic Permission denied");
            }
        }];
        
    
    
    } else if(authStatus == AVAuthorizationStatusDenied){
        NSLog(@"Log: AVAuthorizationStatusDenied %@", mediaType);
        [self noPermissionsCamera];
      // denied
    } else if(authStatus == AVAuthorizationStatusRestricted){
         NSLog(@"Log: AVAuthorizationStatusRestricted %@", mediaType);
        [self noPermissionsCamera];
      // restricted, normally won't happen
    } else if(authStatus == AVAuthorizationStatusNotDetermined){
      // not determined?!
      [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
        if(granted){
          NSLog(@"Log: Granted access to %@", mediaType);
              [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
                        if (granted) {
                            NSLog(@"Mic Permission granted");
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [self openCamera];
                            });
                        }
                        else {
                            [self noPermissionsMicrophoneWithAction:^(){
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self openCamera];
                                });
                            }];
                            NSLog(@"Mic Permission denied");
                        }
                    }];
        } else {
          NSLog(@"Log: Not granted access to %@", mediaType);
            [self noPermissionsCamera];
        }
      }];
    } else {
        [self noPermissionsCamera];
         NSLog(@"Log: impossible, unknown authorization status %@", mediaType);
      // impossible, unknown authorization status
    }
   

}

RCT_EXPORT_METHOD(sampleMethod:(NSString *)stringArgument numberParameter:(nonnull NSNumber *)numberArgument callback:(RCTResponseSenderBlock)callback)
{
    // TODO: Implement
}

@end

import { NativeModules } from 'react-native';

const { RNStoryRecorder } = NativeModules;

export default RNStoryRecorder;

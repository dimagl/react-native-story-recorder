# react-native-story-recorder

## Getting started

`$ npm install react-native-story-recorder --save`

### Mostly automatic installation

`$ react-native link react-native-story-recorder`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-story-recorder` and add `RNStoryRecorder.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNStoryRecorder.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Go to `Build Phases` -> `Copy Bundle Resources`, click `+`, click `Add Other` and select `../node_modules/react-native-story-recorder/ios/StoryRecorder.storyboard`
5. Go to `Build Phases` -> `Copy Bundle Resources`, click `+`, click `Add Other` and select `../node_modules/react-native-story-recorder/ios/RNStoryRecorderAssets.xcassets`
6. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainApplication.java`
  - Add `import com.uloop.storyrecorder.RNStoryRecorderPackage;` to the imports at the top of the file
  - Add `new RNStoryRecorderPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-story-recorder'
  	project(':react-native-story-recorder').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-story-recorder/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-story-recorder')
  	```


## Usage
```javascript
import RNStoryRecorder from 'react-native-story-recorder';

// TODO: What to do with the module?
RNStoryRecorder;
```
  